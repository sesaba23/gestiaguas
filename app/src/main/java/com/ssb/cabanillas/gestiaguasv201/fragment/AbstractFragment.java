package com.ssb.cabanillas.gestiaguasv201.fragment;


import android.animation.Animator;
import android.annotation.TargetApi;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;
import com.echo.holographlibrary.*;
import com.ssb.cabanillas.gestiaguasv201.R;
import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class AbstractFragment extends Fragment {

    private SQLiteDatabase db; //Reference to the data base
    private Cursor cdr; // Reference to resultset to store streets

    private BarGraph bg;
    ArrayList<Integer> suppliesValues = new ArrayList<>(); //Store information about supplies to show in bar graph

    public AbstractFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_abstract, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();

        if(view != null) {
            try {
                /* Connect to de data base in order to show a street list */
                SQLiteOpenHelper supplyDataBaseHelper = new CSupplyDataBaseHelper(getActivity());
                db = supplyDataBaseHelper.getWritableDatabase(); // Get the reference to a data base

                /* Calculate the total number of supplies and supplies completed group by "PG", "PAR" and "IMPAR"*/
                cdr = db.rawQuery("SELECT " + DatabaseContract.Supplies.UWPAR
                        + ", COUNT(NUM_CONTADOR), COUNT(UWFACT) FROM SUPPLIES GROUP BY "
                        + DatabaseContract.Supplies.UWPAR, null);

                //Store number of supplies and number of supplies completed. 1.PG, 2.EVEN, 3.ODD

                while (cdr.moveToNext()){
                    suppliesValues.add(cdr.getInt(2));
                    suppliesValues.add(cdr.getInt(1));
                }
                //Calculate and supplies completed  and the total number of supplies
                suppliesValues.add(suppliesValues.get(0)+ suppliesValues.get(2) + suppliesValues.get(4));
                suppliesValues.add(suppliesValues.get(1) + suppliesValues.get(3) + suppliesValues.get(5));
                //Show text abstract about supplies read and total number of supplies
                TextView totalStreets = (TextView) view.findViewById(R.id.total_number_supplies);
                totalStreets.setText("Lecturas Completadas: " + suppliesValues.get(6) + "/" + suppliesValues.get(7));

                /* Calculate the number of streets */
                cdr = db.rawQuery("SELECT DISTINCT NOMBRE_CALLE FROM SUPPLIES", null);
                cdr.moveToLast();
                int numStreets = cdr.getCount();

                /* Calculate the number of streets marked as completed... */
                cdr = db.rawQuery("SELECT COUNT(COMPLETED) FROM STREETS WHERE COMPLETED = 1", null);
                cdr.moveToFirst();
                int numStreetsCompleted = cdr.getInt(0);

                /* ...And show next to pie chart */
                TextView completed = (TextView) view.findViewById(R.id.total_number_streets);
                completed.setText(numStreetsCompleted + " Calles leídas");
                TextView totalSupplies = (TextView) view.findViewById(R.id.total_number_completed_streets);
                totalSupplies.setText(numStreets + " Calles");

                /* Calculate the starting and last save record date*/
                String blank = "";
                cdr = db.rawQuery("SELECT MAX(UWFACT), MIN(UWFACT) FROM SUPPLIES", null);
                /* If there is any lecture already save...*/
                if(cdr.moveToFirst()) {
                    //Only populate fields if almost one supply has been read
                    if (cdr.getString(0) != null | cdr.getString(1) != null) {
                        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        Date timeMax = null, timeMin = null;
                        try {
                            timeMax = format.parse(cdr.getString(0));
                            timeMin = format.parse(cdr.getString(1));
                        } catch (ParseException | NullPointerException e) {
                            e.printStackTrace();
                        }
                        long totalTimeMilliseconds = (timeMin.getTime() - timeMax.getTime());
//                       String time = getDate(totalTimeMilliseconds, "dd HH:mm:ss");
                        long totalTimeHours = totalTimeMilliseconds % (long)(1000 * 3600 * 24);
                        totalTimeHours = Math.abs(totalTimeHours / (1000 * 3600));
                        long totalTimeDays = Math.abs(totalTimeMilliseconds / (1000 * 3600 * 24));

                        //Show information about time
                        TextView totalTime = (TextView) view.findViewById(R.id.total_time);
                        totalTime.setText("Tiempo Empleado: " + totalTimeDays +" días y " + totalTimeHours + " horas");
                        TextView startDate = (TextView) view.findViewById(R.id.start_date);
                        startDate.setText("Primera lectura: " + cdr.getString(1));
                        TextView endDate = (TextView) view.findViewById(R.id.end_date);
                        endDate.setText("Última lectura: " + cdr.getString(0));
                    }
                }

                /**
                 *  Show and configure and animate PIE CHART showing statistics
                 **/
                PieGraph pg = (PieGraph)view.findViewById(R.id.graph);
                /*Reset the pie Chart every time we enter the drawer*/
                if(pg.getSlices().size() != 0){
                    pg.removeSlices();
                }
                PieSlice slice = new PieSlice();
                slice.setColor(getResources().getColor(R.color.colorPrimaryDark));
                slice.setValue(numStreets);//->total number of supplies but if no motion: numCompleted
                slice.setGoalValue((float) numStreetsCompleted);//->Supplies Completed
                pg.addSlice(slice);
                slice = new PieSlice();
                slice.setColor(getResources().getColor(R.color.colorAccent));
                slice.setValue((0));//from 0... but if no motion: numSupplies - numCompleted
                slice.setGoalValue((float) (numStreets - numStreetsCompleted));//...to total number of supplies subtract supplies completed
                pg.addSlice(slice);
                pg.setInnerCircleRatio(0);
                pg.setPadding(0);
                //Configure the animation of chart
                pg.setDuration(3000);//default if unspecified is 300 ms
                pg.setInterpolator(new AccelerateDecelerateInterpolator());//default if unspecified is linear; constant speed
//                pg.setAnimationListener(getAnimationListener());//optional
                pg.animateToGoalValues();

                /**
                 *  Configure and animate BAR GRAPH showing statistics
                 **/
                String[] texts = {"Par", "Total", "Impar", "Total", "Pg", "Total", "Leídos", "Total"};
                ArrayList<Bar> points = new ArrayList<>();
                Bar bar;
                for(int i = 0; i < texts.length; i++){
                    bar = new Bar();
                    bar.setName(texts[i]);
                    if(i % 2 == 0)
                        bar.setColor(getResources().getColor(R.color.colorPrimaryDark));
                    else
                        bar.setColor(getResources().getColor(R.color.colorAccent));
                    bar.setValue(1000);
                    bar.setGoalValue(suppliesValues.get(i));
                    points.add(bar);
                }

                final BarGraph g = (BarGraph)view.findViewById(R.id.bar_graph);
                bg = g;
                g.setShowBarText(true);
                g.setShowAxis(true);
                g.setBars(points);
                g.setDuration(3000);//default if unspecified is 300 ms
                g.setInterpolator(new AccelerateDecelerateInterpolator());//Only use over/undershoot  when not inserting/deleting
                g.setValueStringPrecision(0); //1 decimal place. 0 by default for integers.
                g.setAnimationListener(getAnimationListener());
                g.animateToGoalValues();
            }catch (SQLiteException e) {
            /* If access to the data base fails show a message*/
                Toast toast = Toast.makeText(getActivity(), "Todavía no se ha introducido ninguna lectura", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public Animator.AnimatorListener getAnimationListener(){
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            return new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    ArrayList<Bar> newBars = new ArrayList<Bar>();
                    //Keep bars that were not deleted
                    for (Bar b : bg.getBars()){
                        if (b.mAnimateSpecial != HoloGraphAnimate.ANIMATE_DELETE){
                            b.mAnimateSpecial = HoloGraphAnimate.ANIMATE_NORMAL;
                            newBars.add(b);
                        }
                    }
                    bg.setBars(newBars);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            };
        else return null;

    }

    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
