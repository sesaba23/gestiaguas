package com.ssb.cabanillas.gestiaguasv201.database;

/**
 * Created by sergio on 11/04/16.
 *
 * Defines the name of the database and the name for all the tables and columns as constants
 * It serves as an idea of the structure of the database and avoid mistakes allowing us to make
 * only one change in the value of the constant and propagate over our entire app
 */
public class DatabaseContract {

    public static final String DB_NAME = "gestiaguas_supplies"; //The name of the data base"

    /**
     * Static Global Variables to share information between Intents
     **/
    public static final String SELECTEDABREVTIPOVIA = "abrevTipoVia"; //Store selected short of street in TopLevelActivity
    public static final String SELECTEDSTREET = "selectedStreet"; //Store selected street clicked in TopLevelActivity
    public static final String EXTRA_SUPPLYNO = "supplyNo"; //Store the supply selected to show in SupplyActivity
    public static final String ARRAY_SUPPLYNO = "supplyNoList"; //Store the supplies' list of a street

    /**
     * Types of streets
     */
    public static final String PG = "PG";
    public static final String PAR = "PAR";
    public static final String IMPAR = "IMPAR";

    /**
     * Database streets
     */
    public abstract class Streets{
        public static final String TABLE_NAME = "STREETS";
        public static final String _ID = "_id";
        public static final String ABREV_TIPO_VIA = "ABREV_TIPO_VIA";
        public static final String NOMBRE_CALLE = "NOMBRE_CALLE";
        public static final String URBANIZACION = "URBANIZACION";
        public static final String NUM_COMPLETED = "NUM_COMPLETED";
        public static final String COMPLETED = "COMPLETED";
        public static final String NUM_SUPPLIES = "NUM_SUPPLIES";
    }

    /**
     * Database supplies
     */
    public abstract class Supplies {
        public static final String TABLE_NAME = "SUPPLIES";
        public static final String _ID = "_id";
        public static final String ID = "ID";
        public static final String ABREV_TIPO_VIA = "ABREV_TIPO_VIA";
        public static final String NOMBRE_CALLE = "NOMBRE_CALLE";
        public static final String NUM = "NUM";
        public static final String LOCAL = "LOCAL";
        public static final String PORTAL = "PORTAL";
        public static final String BLOQUE = "BLOQUE";
        public static final String ESCALERA = "ESCALERA";
        public static final String PLANTA = "PLANTA";
        public static final String LETRA = "LETRA";
        public static final String UWPAR = "UWPAR";
        public static final String NUM_CONTADOR = "NUM_CONTADOR";
        public static final String CALIBRE_MM = "CALIBRE_MM";
        public static final String UWLANT = "UWLANT";
        public static final String UWFACT = "UWFACT";
        public static final String UWLACT = "UWLACT";
        public static final String CONSUMO = "CONSUMO";
        public static final String ABREV_ESTADO = "ABREV_ESTADO";
        public static final String ESTADOS = "ESTADOS";
    }

    /**
     * Database state_supplies
     */
    public abstract class States {
        public static final String TABLE_NAME = "STATE_SUPPLIES";
        public static final String _ID = "_id";
        public static final String ABREV_ESTADO = "ABREV_ESTADO";
        public static final String ESTADOS = "ESTADOS";
    }
}
