package com.ssb.cabanillas.gestiaguasv201.backup;

/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

        import android.os.Bundle;
        import android.util.Log;

        import com.google.android.gms.common.api.ResultCallback;
        import com.google.android.gms.drive.Drive;
        import com.google.android.gms.drive.DriveApi.DriveContentsResult;
        import com.google.android.gms.drive.DriveContents;
        import com.google.android.gms.drive.DriveFolder.DriveFileResult;
        import com.google.android.gms.drive.MetadataChangeSet;
        import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;
        import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;

        import java.io.File;
        import java.io.FileInputStream;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.OutputStream;
        import java.io.OutputStreamWriter;
        import java.io.Writer;
        import java.text.SimpleDateFormat;
        import java.util.Date;

/**
 * An activity to illustrate how to create a file.
 */
public class CreateFileActivity extends BaseDemoActivity {

    private static final String TAG = "CreateFileActivity";

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);
        // create new contents resource
        Drive.DriveApi.newDriveContents(getGoogleApiClient())
                .setResultCallback(driveContentsCallback);
    }

    final private ResultCallback<DriveContentsResult> driveContentsCallback = new
            ResultCallback<DriveContentsResult>() {
                @Override
                public void onResult(DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        showMessage("Error while trying to create new file contents");
                        return;
                    }
                    final DriveContents driveContents = result.getDriveContents();

                    // Perform I/O off the UI thread.
                    new Thread() {
                        @Override
                        public void run() {
                            // write content to DriveContents
                            OutputStream outputStream = driveContents.getOutputStream();
                            Writer writer = new OutputStreamWriter(outputStream);
                            try {
//                                writer.write("Hello World!");
//                                writer.close();



                                String directory = "/storage/emulated/0/Gestiaguas";
                                File d = new File(directory);
                                if(!d.exists()){
                                    d.mkdir();
                                    System.out.println("Directorio Creado");
                                }

                                final String inFileName = "/data/data/com.ssb.cabanillas.gestiaguasv201/databases/"
                                        + DatabaseContract.DB_NAME;
                                File dbFile = new File(inFileName);

                                FileInputStream fileInputStream = new FileInputStream(dbFile);



                                byte[] buffer = new byte[1024];
                                int length;
                                while((length = fileInputStream.read(buffer)) > 0){
                                    outputStream.write(buffer, 0, length);
                                }

                                outputStream.flush();
                                outputStream.close();
                                fileInputStream.close();





                            } catch (IOException e) {
                                Log.e(TAG, e.getMessage());
                            }

                            String timeStamp = new SimpleDateFormat("ddmmyyyy_hhmmss").format(new Date());

                            MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                    .setTitle("BackUp_" + DatabaseContract.DB_NAME + "_" + timeStamp)
//                                    .setMimeType("text/plain")
                                    .setMimeType("text/csv")
                                    .setStarred(true).build();

                            // create a file on root folder
                            Drive.DriveApi.getRootFolder(getGoogleApiClient())
                                    .createFile(getGoogleApiClient(), changeSet, driveContents)
                                    .setResultCallback(fileCallback);
                        }
                    }.start();

                }
            };

    final private ResultCallback<DriveFileResult> fileCallback = new
            ResultCallback<DriveFileResult>() {
                @Override
                public void onResult(DriveFileResult result) {
                    if (!result.getStatus().isSuccess()) {
                        showMessage("Error while trying to create the file");
                        return;
                    }
                    showMessage("Created a file with content: " + result.getDriveFile().getDriveId());

                    //FinishActivity
                    finish();
                }
            };


    private void crearBackUp() throws IOException{
        String timeStamp = new SimpleDateFormat("ddmmyyyy_hhmmss").format(new Date());

        String directory = "/storage/emulated/0/Gestiaguas";
        File d = new File(directory);
        if(!d.exists()){
            d.mkdir();
            System.out.println("Directorio Creado");
        }

        final String inFileName = "/data/data/com.ssb.cabanillas.gestiaguasv201/databases/"
                + DatabaseContract.DB_NAME;
        File dbFile = new File(inFileName);

        FileInputStream fileInputStream = new FileInputStream(dbFile);


        String outputFileName = directory + "/" + DatabaseContract.DB_NAME + "_" + timeStamp;

        OutputStream output = new FileOutputStream(outputFileName);

        byte[] buffer = new byte[1024];
        int length;
        while((length = fileInputStream.read(buffer)) > 0){
            output.write(buffer, 0, length);
        }

        output.flush();
        output.close();
        fileInputStream.close();
    }

}