package com.ssb.cabanillas.gestiaguasv201;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;
import com.ssb.cabanillas.gestiaguasv201.fragment.SupplyCategoryFragment;
import com.ssb.cabanillas.gestiaguasv201.fragment.SupplyFragment;

import java.util.ArrayList;

public class SupplyCategoryActivity extends AppCompatActivity implements SupplyCategoryFragment.supplyListListener {

    private String selectedAbrevTipoVia;
    private String selectedStreet;

    private SupplyCategoryFragment supplyCategoryFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_supply);

        /* We set the up button in the action bar*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Get the  abrevTipoVia and selectedStreet from the intent
        selectedAbrevTipoVia = (String) getIntent().getExtras().get(DatabaseContract.SELECTEDABREVTIPOVIA);
        selectedStreet = (String) getIntent().getExtras().get(DatabaseContract.SELECTEDSTREET);
        //Populate the street name in the action bar
        setTitle(selectedAbrevTipoVia + "/ " + selectedStreet);

        /* Show SupplyCategoryFragment inside "list_frag" FrameLayout of SupplyCategoryAtivity*/
        supplyCategoryFragment = new SupplyCategoryFragment();
        getFragmentManager().beginTransaction().replace(R.id.list_frag, supplyCategoryFragment).addToBackStack(null).commit();

    }

    @Override
    public void itemClicked(long id) {

        View fragmentContainer = findViewById(R.id.fragment_container);
        /* If the view of activity_category_supply contains the fragment to show supplies we are in a tablet
        * so we show together the  list of supplies and the supply selected...*/
        if(fragmentContainer != null) {
            //The code to set the detail will go here
            SupplyFragment details = new SupplyFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            details.setSupplyNo((int) id);
            ft.replace(R.id.fragment_container, details);
            ft.addToBackStack(LAYOUT_INFLATER_SERVICE);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
        /*...If it doesn't we are in a phone and we need to call the detail activity*/
        else{
            SupplyCategoryFragment frag = (SupplyCategoryFragment)getFragmentManager().findFragmentById(R.id.list_frag);
            //Clear the array of number supplies
            ArrayList<Integer> supplyNoList = frag.getSupplyNoList();

            Intent intent = new Intent(this, SupplyActivity.class);
            intent.putExtra(DatabaseContract.EXTRA_SUPPLYNO, (int)id);
            intent.putExtra(DatabaseContract.ARRAY_SUPPLYNO, supplyNoList);
            intent.putExtra(DatabaseContract.SELECTEDSTREET, selectedStreet);
            startActivity(intent);
        }
    }

    /* We implement de back button when it is selected because we are in compatibility mode*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                //app icon in action bar clicked; go to parent activity
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}