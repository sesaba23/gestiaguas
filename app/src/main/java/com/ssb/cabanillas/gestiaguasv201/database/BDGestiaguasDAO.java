package com.ssb.cabanillas.gestiaguasv201.database;

import java.sql.SQLException;

/**
 * Clase que realiza la conexión a la Base de Datos y Proporciona los métodos necesarios
 * para interactuar con la misma
 *
 * ¡¡¡ OJO !!! No olvidar añadir el driver jDBC a las librerias del proyecto
 *
 * @author Sergio Sánchez Barahona
 * @version Gestiaguas v2.0
 * @since 14/12/2014
 */
public class BDGestiaguasDAO {

    private java.sql.Connection conexion;
    private java.sql.Statement sentenciaSQL;
    private java.sql.ResultSet cdr; // conjunto de resultados

    /* Almacena la Clave y el usuario para acceder a la base de datos*/
    private String usuario = new String();
    private String password = new String();

    /**
     * Constructor de pruebas que realiza la conexión a la Base de Datos y Proporciona los métodos necesarios
     * NO es necesario introducir usuario y contraseña
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public BDGestiaguasDAO() throws ClassNotFoundException, SQLException,
            InstantiationException, IllegalAccessException
    {
        // Cargar el controlador JDBC.
        String controlador = "com.mysql.jdbc.Driver";
        Class.forName(controlador).newInstance();

        conectar(); // conectar con la fuente de datos
    }

    /**
     * Método que proporciona la conexión a la Base de Datos Gestiaguas
     * @throws SQLException
     */
    public final void conectar() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        String URL_bd = "jdbc:mysql://192.168.2.161:3306/gestiaguas-java";
        String usuario = "sergio";
        String contraseña = "tusientras08";

        // Conectar con la BD
        conexion = java.sql.DriverManager.getConnection(URL_bd, usuario, contraseña);
        // Crear una sentencia SQL
        sentenciaSQL = conexion.createStatement(
                java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE,
                java.sql.ResultSet.CONCUR_UPDATABLE);

    /* Para realizar pruebas*/
        System.out.println("Conexión realizada con éxito.");
    }

    /**
     * Cierra la conexión a la base de datos liberando recursos
     * @throws SQLException
     */
    public void cerrarConexion() throws SQLException
    {
        System.out.println("Conexión cerrada");
        if (cdr != null) cdr.close();
        if (sentenciaSQL != null) sentenciaSQL.close();
        if (conexion != null) conexion.close();
    }


    /**
     * Método que muestra un listado de las calles del municipio
     * @return ResulSet   Conjunto de Resultados con el listado calles
     * @throws SQLException
     */
    public java.sql.ResultSet obtenerListadoDeCalles() throws SQLException
    {
        cdr = sentenciaSQL.executeQuery("SELECT ABREV_TIPO_VIA, NOMBRE_CALLE, POLIGONO, URBANIZACION FROM"
                + " calles, poligonos, tipos_via WHERE calles.COD_POL = poligonos.COD_POL and "
                + "tipos_via.COD_TIPO_VIA = calles.COD_TIPO_VIA ORDER BY NOMBRE_CALLE");
        return cdr;
    }

    /**
     * Método que muestra el listado de direcciones de los distintos suministros
     * @return ResulSet   Conjunto de Resultados con el listado de direcciones
     * @throws SQLException
     */
    public java.sql.ResultSet obtenerSuministros() throws SQLException
    {

        cdr = sentenciaSQL.executeQuery("SELECT ABREV_TIPO_VIA, NOMBRE_CALLE,"
                + " NUM, LOCAL, PORTAL, BLOQUE, ESCALERA, PLANTA, LETRA, PARCELA AS 'Parcela',"
                + " MODULO AS 'Módulo', NAVE AS 'Nave', NUM_CONTADOR, ABREV_ESTADO, ESTADOS FROM direcciones_suministro,"
                + " tipos_via, calles, dire_cont, contadores, estado_contador WHERE calles.COD_TIPO_VIA = tipos_via.COD_TIPO_VIA and "
                + "direcciones_suministro.COD_CALLE = calles.COD_CALLE and direcciones_suministro.NUM_FIJO = dire_cont.NUM_FIJO and "
                + "dire_cont.ID_CONTADOR = contadores.ID_CONTADOR and estado_contador.COD_ESTADOS = contadores.COD_ESTADOS "
                + "ORDER BY NOMBRE_CALLE, NUM, LOCAL, PORTAL, BLOQUE, ESCALERA, PLANTA, LETRA" );
        return cdr;
    }


    /**
     * Método que muestra el listado de direcciones de los distintos suministros
     * @return ResulSet   Conjunto de Resultados con el listado de direcciones
     * @throws SQLException
     */
    public java.sql.ResultSet obtenerListadoDirecciones() throws SQLException
    {

        cdr = sentenciaSQL.executeQuery("SELECT NUM_FIJO AS 'Número Fijo', REF_CATASTRAL AS 'Ref. Catastral', ABREV_TIPO_VIA AS 'Tipo Vía', NOMBRE_CALLE AS 'Calle',"
                + " NUM AS 'Núm.', LOCAL AS 'Loc.', PORTAL AS 'Portal', BLOQUE AS 'Bloq.', ESCALERA AS 'Esc.', PLANTA AS 'Planta', LETRA AS 'Letra', PARCELA AS 'Parcela',"
                + " MODULO AS 'Módulo', NAVE AS 'Nave' FROM direcciones_suministro,"
                + " tipos_via, calles WHERE calles.COD_TIPO_VIA = tipos_via.COD_TIPO_VIA and "
                + "direcciones_suministro.COD_CALLE = calles.COD_CALLE ORDER BY NOMBRE_CALLE, NUM, LOCAL, PORTAL, BLOQUE, ESCALERA, PLANTA, LETRA" );
        return cdr;
    }

    /**
     * Método que muestra el listado de contadores existentes en la Base de Datos
     * @return ResulSet   Conjunto de Resultados con el listado de contadores
     */
    public java.sql.ResultSet obtenerListadoContadores() throws SQLException
    {
        cdr = sentenciaSQL.executeQuery("SELECT NUM_FIJO AS 'Número Fijo', ID_CONTADOR AS 'ID Contador', NUM_CONTADOR AS 'Nº Contador', CALIBRE_PULGADAS"
                + " AS 'Pulgadas', CALIBRE_MM AS 'mm', ESTADOS AS 'Estado', FECHA_ESTADO AS 'Fecha Estado', TIPO AS 'Tipo' FROM dire_cont"
                + " LEFT JOIN contadores USING (ID_CONTADOR) LEFT JOIN calibres USING (COD_CALIBRE) LEFT JOIN estado_contador"
                + " USING (COD_ESTADOS) LEFT JOIN tipos_contador USING (COD_TIPO)");
        return cdr;
    }

    /**
     * Método que muestra el listado de los estados posibles que puede tener un contador
     * @return ResulSet   Conjunto de Resultados con el listado de estados
     */
    public java.sql.ResultSet obtenerEstados() throws SQLException
    {
        cdr = sentenciaSQL.executeQuery("SELECT ESTADOS, ABREV_ESTADO FROM estado_contador");
        return cdr;
    }


}