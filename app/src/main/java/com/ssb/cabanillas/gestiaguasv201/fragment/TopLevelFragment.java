package com.ssb.cabanillas.gestiaguasv201.fragment;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.ListView;

import com.ssb.cabanillas.gestiaguasv201.adapter.MyStreetCursorAdapter;
import com.ssb.cabanillas.gestiaguasv201.R;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;
import com.ssb.cabanillas.gestiaguasv201.database.GAProvider;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopLevelFragment extends Fragment implements android.app.LoaderManager.LoaderCallbacks<Cursor>{

    private ListView listView; //Stores the listview of the fragment_top_level

    public static final int GA_STREETS_LOADER =1;

    private ContentResolver cr;
    private MyStreetCursorAdapter mAdapter;

    private Bundle bundle;

    private String[] projection = new String[]{DatabaseContract.Streets._ID,
            DatabaseContract.Streets.ABREV_TIPO_VIA,
            DatabaseContract.Streets.NOMBRE_CALLE,
            DatabaseContract.Streets.URBANIZACION,
            DatabaseContract.Streets.COMPLETED,
            DatabaseContract.Streets.NUM_COMPLETED,
            DatabaseContract.Streets.NUM_SUPPLIES};

    private Filter filter;

    private String streetsToShow = "%"; // Defines if completed or pending streets are shown in the layout

    public TopLevelFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /* We can not use getView() because View has not been created yet by onCreateView
        * Instead, we ask for the inflater view*/
        View rootView = inflater.inflate(R.layout.fragment_top_level, container, false);
        Log.d("TopLevelFragment", "Entering TopLevelFragment");
        listView = (ListView) rootView.findViewById(R.id.street_list);

        cr = getActivity().getContentResolver();

        mAdapter = new MyStreetCursorAdapter(getActivity(), null, 0, streetsToShow);

        listView.setAdapter(mAdapter);

        //Choose if you want to show pending/completed/all streets
        bundle = new Bundle();
        bundle.putString(DatabaseContract.Streets.COMPLETED, streetsToShow);

        //Initialize LoaderManager
        getLoaderManager().initLoader(GA_STREETS_LOADER, bundle, this);

        Log.d("TopLevelFragment", "Streets from content Provider read");

        // Inflate the layout for this fragment
        return rootView;
    }

    /* Every time we load the fragment, update de list view street information*/
    @Override
    public void onStart() {
        super.onStart();

        //Restored the previous position if we come back from categoryActivity
        listView.setSelectionFromTop(mAdapter.getPositionList(), 0);
    }

    /* Close de Cursor and de dataBase when exit the activity*/
    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    /* Set filter if typing text, otherwise reset the filter*/
    public void setSearch(String newText) {

        //Restart loader in order to implement filter using content provider an cursorLoaders
        bundle.putString("NEWTEXT", newText);
        getLoaderManager().restartLoader(GA_STREETS_LOADER, bundle, this);
    }

    /* Set if completed streets or/and pending streets are selected. 0-->completed, 1-->pending, -1-->both */
    public void setStreetsToShow(String streetsToShow) {
        this.streetsToShow = streetsToShow;
    }

    /**
     * Override methods of Loader class in order to implement CursorLoader with contentProvider
     */
    @Override
    public android.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //Selected if we want to show completed/pending/all streets
        String completed = args.getString(DatabaseContract.Streets.COMPLETED);
        //Look for the text typed inside search field. It can bi streets, abrev_tipo_via and urbanizacion
        String newText = args.getString("NEWTEXT");
        //If there is no text type look for every existing street
        if(TextUtils.isEmpty(newText))
        { newText  = "";}

        //Return Cursor Loader with the correct information
        switch (id) {
            case GA_STREETS_LOADER:
                return new android.content.CursorLoader(
                        getActivity(), //Parent Activity context
                        Uri.parse(GAProvider.STREETS_TABLE_URI), //Table to query
                        projection,    //Projection to return
                        DatabaseContract.Streets.COMPLETED + " LIKE ?"
                               + " AND (" + DatabaseContract.Streets.ABREV_TIPO_VIA + " LIKE ?"
                               + " OR " + DatabaseContract.Streets.NOMBRE_CALLE + " LIKE ?"
                               + " OR " + DatabaseContract.Streets.URBANIZACION + " LIKE ?)",          //selection clause
                        new String[]{completed, newText + "%", newText + "%", newText + "%"},          //selection arguments
                        null           //Default sort order
                );
            default:
                //Invalid ID
                return null;
        }
    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {
        mAdapter.changeCursor(data);
        Log.d("TopLevelFragment", "OnLoader Finished: " + data.getCount());
    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
        Log.d("TopLevelFragment", "OnLoader Reset. ");
    }

    /**
     * NOT USE. In order to show how to use setFilterQueryProvider if we don't want to
     * implement FilterQueryProvider in the cursorAdapter class when we are not using Cursor Loader
     */
    private void setMyFilter(){
        //Enable filtering in the listview
        listView.setTextFilterEnabled(true);
        mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                String strItemCode;
                if(constraint != null)
                    strItemCode= constraint.toString();
                else
                    strItemCode = "";

                Uri uriPath = Uri.parse(GAProvider.STREETS_TABLE_URI);
                return cr.query(uriPath,                                                //Content Provider Table
                        null,                                                           //Projection
                        DatabaseContract.Streets.NOMBRE_CALLE + " LIKE ? "              //Selection
                                + "OR " + DatabaseContract.Streets.ABREV_TIPO_VIA + " LIKE ? "
                                + "OR " + DatabaseContract.Streets.URBANIZACION + " LIKE ?",
                        new String[] {strItemCode + "%", strItemCode + "%", strItemCode + "%" },//Selection Arguments
                        null);                                                          //Sort Order

            }
        });
        filter = mAdapter.getFilter(); // Placing the adapter to a separate filter remove the text popup
    }

}


