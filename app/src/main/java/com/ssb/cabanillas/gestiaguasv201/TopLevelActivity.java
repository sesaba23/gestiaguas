package com.ssb.cabanillas.gestiaguasv201;
import com.ssb.cabanillas.gestiaguasv201.backup.*;
import com.ssb.cabanillas.gestiaguasv201.fragment.AbstractFragment;
import com.ssb.cabanillas.gestiaguasv201.fragment.TopLevelFragment;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.SearchView;
import android.widget.Toast;
import java.io.IOException;

public class TopLevelActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    /*The search icon in the actionBar*/
    private SearchView inputSearch;
    private MenuItem searchItem, settingsItem,aboutItem;

    private long doubleBackToExitPressedOnce;

    private static final int TIME_INTERVAL = 2000; //ms, desired time passed between two back presses.
    Toast toast = null; //To show message on one backbuttom pressed

    private String auxText; //Stores the text introduced to search for a street

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);

        //Display the FrameLayout with shows all streets by default
        TopLevelFragment topLevelFragment = new TopLevelFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        /* code '-1' means we want to show all streets, '0' we want completed and '1' we want only pending streets to de shown*/
        topLevelFragment.setStreetsToShow("%");
        ft.replace(R.id.top_level_frag, topLevelFragment)
                .addToBackStack(LAYOUT_INFLATER_SERVICE)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                AbstractFragment af = new AbstractFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.replace(R.id.street_list_drawer, af).addToBackStack(LAYOUT_INFLATER_SERVICE).commit();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        // Set the capacity of show completed ando/or pendings streets
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    /* We need to reload filter after the cursor is updated and the adapter is set in the fragment in its onStart() method*/
    @Override
    protected void onResume() {
        super.onResume();

        //If we come back from another activity and  there is some text in the search field, set the filter in the fragment
        //so we don't lose previous searches
        TopLevelFragment frag = (TopLevelFragment) getFragmentManager().findFragmentById(R.id.top_level_frag);
        if (!TextUtils.isEmpty(auxText)) {
            frag.setSearch(auxText);
        }
    }

    /* Back up Data Base in an accessible directory when exit the activity*/
    @Override
    public void onDestroy(){
        super.onDestroy();

        /* Launch the google drive backup */
        Intent intent = new Intent(getBaseContext(), CBackUpDataBase.class);
        startActivity(intent);

        try {
            CBackUpDataBase.createDataBaseBackUp();
            System.out.println("Base de datos respaldada correctamente");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Only close aplication if back button is pressed twice*/
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else {
            if(doubleBackToExitPressedOnce + TIME_INTERVAL > System.currentTimeMillis()) {
                toast.cancel();
                super.onBackPressed();
                return;
            }else {
                 toast = Toast.makeText(this, R.string.text_on_back_pressed_to_exit, Toast.LENGTH_SHORT);
                 toast.show();
            }
            doubleBackToExitPressedOnce = System.currentTimeMillis();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.top_level, menu);

        settingsItem = menu.findItem(R.id.action_settings);
        aboutItem = menu.findItem(R.id.action_about);
        /* Load search icon on actionbar*/
        searchItem = menu.findItem(R.id.action_search);
        inputSearch = (SearchView) MenuItemCompat.getActionView(searchItem);
        inputSearch.setQueryHint(getString(R.string.action_search_hint));

        /* Always show settings icon item when we don't want to to search a street any more
        * We need compatibility and MenuItemCompat is static so, be careful!!!! */
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            //If user presses search icon don't show settings icon...
            public boolean onMenuItemActionExpand(MenuItem item) {
                settingsItem.setVisible(false);
                aboutItem.setVisible(false);
                return true;
            }

            // If user closes search bar, show settings icon again...
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                settingsItem.setVisible(true);
                aboutItem.setVisible(true);
                return true;
            }
        });
         /* When user clicks on search icon Activate search filter*/
        inputSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            //When type text pass it to the fragment so we can look for street
            @Override
            public boolean onQueryTextChange(String newText) {
                TopLevelFragment frag = (TopLevelFragment) getFragmentManager().findFragmentById(R.id.top_level_frag);
                frag.setSearch(newText);
                auxText = newText; //save the text typed in the filter to reuse when back from another activity
                return false;
            }
        });
        return true;
    }

    /* If we press one icon of the actionbar start the correct activity*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
             /* Open preference Activity*/
            Intent intent = new Intent(this,Settings_Activity.class);
            startActivity(intent);
            return true;
        }
        if(id == R.id.action_about){
             /* Open about Activity*/
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }
        if(id == R.id.action_search){

        }
        if(id == android.R.id.home){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //Load TopLevel fragment
        TopLevelFragment topLevelFragment = new TopLevelFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // When user wants to show only pending streets...
        if (id == R.id.nav_show_completed) {
            // If it has already checked turn to false and show all streets
            if(item.isChecked()){
                item.setChecked(false);
                topLevelFragment.setStreetsToShow("%");
            } // If it is checked show only completed streets
            else {
                item.setChecked(true);
                topLevelFragment.setStreetsToShow("1");
            }
        // When user wants to show only completed streets...
        } else if (id == R.id.nav_show_pending) {
            if(item.isChecked()){
                item.setChecked(false);
                topLevelFragment.setStreetsToShow("%");
            }else {
                item.setChecked(true);
                topLevelFragment.setStreetsToShow("0");
            }
        }
        //Then load a new fragment
        ft.replace(R.id.top_level_frag, topLevelFragment);
        ft.addToBackStack(LAYOUT_INFLATER_SERVICE);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();

        // Close the drawer automatically after clicked some option
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
