package com.ssb.cabanillas.gestiaguasv201;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;
import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseSQLUpdate;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;

public class Settings_Activity extends AppCompatActivity {

    public SQLiteDatabase db; //Reference to the data base
    private Cursor cdr; // Reference to resultset to store queries

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle(R.string.settings_screen);
    }

    /* Call when the button "Download BD" is clicked in preference screen
    * Deletes database and download again the full SQL Data Base*/
    public void onClickDownloadDataBase(View view) {

        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();

        /* Read ssid name preference*/
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String ssidName = sharedPreferences.getString("preferences_ssid", "Vodafone");

        /*Check if there is a correct wifi connection*/
        if (wifi.isWifiEnabled() & info.getSSID().contains(ssidName)) {
            try {
                System.out.println("downloadFlag puesto a true");
            /* Delete DataBase an create an other one*/
                this.deleteDatabase(DatabaseContract.DB_NAME);

            /* Connect with the data base*/
                SQLiteOpenHelper supplyDataBaseHelper = new CSupplyDataBaseHelper(this);
                db = supplyDataBaseHelper.getWritableDatabase(); // Get the reference to a data base

            } catch (SQLiteException e) {
            /* If access to the data base fails show a message*/
                Toast toast = Toast.makeText(this, "Data Base unavailable", Toast.LENGTH_SHORT);
                toast.show();
            }
        }else{
            Toast toast = Toast.makeText(this, R.string.wifi_not_enable, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /* Call when the button "Update BD" is clicked in preference screen
    * Update the SQL data Base only with register that have been changed*/
    public void onClickUpdateDataBase(View view) {

        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();

        /* Read ssid name preference*/
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String ssidName = sharedPreferences.getString("preferences_ssid", "Vodafone");

        /*Check if there is a correct wifi connection*/
        if (wifi.isWifiEnabled() & info.getSSID().contains(ssidName)) {
            try {
            /* Connect with the data base*/
                SQLiteOpenHelper supplyDataBaseHelper = new CSupplyDataBaseHelper(this);
                db = supplyDataBaseHelper.getReadableDatabase(); // Get the reference to a data base

                /* Show only the supplies that have been changed and have been read*/
                cdr = db.rawQuery("SELECT ID, UWFACT, UWLACT, CONSUMO, NUM_CONTADOR, CALIBRE_MM, " +
                        " ESTADOS FROM SUPPLIES WHERE UWFACT IS NOT NULL", null);
                while(cdr.moveToNext()){
                    System.out.println("IDs Leídos: " + cdr.getInt(0));
                }
                cdr.moveToFirst();
                cdr.moveToPrevious();
                CSupplyDataBaseSQLUpdate BDUpdate = new CSupplyDataBaseSQLUpdate(this);
                BDUpdate.actualizarDatosBDSQL(cdr);


            } catch (SQLiteException e) {
            /* If access to the data base fails show a message*/
                Toast toast = Toast.makeText(this, "Data Base unavailable", Toast.LENGTH_SHORT);
                toast.show();
            }
        }else{
            Toast toast = Toast.makeText(this, R.string.wifi_not_enable, Toast.LENGTH_LONG);
            toast.show();
        }

    }

}


