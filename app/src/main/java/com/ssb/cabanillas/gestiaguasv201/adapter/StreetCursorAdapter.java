package com.ssb.cabanillas.gestiaguasv201.adapter;

import android.app.VoiceInteractor;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.ssb.cabanillas.gestiaguasv201.R;
import com.ssb.cabanillas.gestiaguasv201.database.CStreet;

import java.util.ArrayList;

/**
 * Created by sergio on 18/04/16.
 */
public class StreetCursorAdapter extends BaseAdapter {

    private Context mContext;
    private Cursor cursor = null;

    public StreetCursorAdapter(Context mContext, Cursor cursor) {
        this.mContext = mContext;
        this.cursor = cursor;
    }

    @Override
    public int getCount() {
        if(cursor != null)
            return cursor.getCount();
        else
            return -1;
    }

    @Override
    public Object getItem(int position) {
        if(cursor != null) {
            cursor.moveToPosition(position);
            return cursor.getPosition();
        }
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        if(cursor != null) {
            cursor.moveToPosition(position);
            return cursor.getInt(0);
        }
        else
            return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View myView;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            myView = inflater.inflate(R.layout.item_listview_street, null);
        }else {
            myView = convertView;
        }

        if(cursor != null) {
            cursor.moveToPosition((int) getItem(position));

            TextView abrevTipoVia = (TextView) myView.findViewById(R.id.abrevtipovia);
            abrevTipoVia.setText(cursor.getString(1));

            TextView calle = (TextView) myView.findViewById(R.id.calle);
            calle.setText(cursor.getString(2));

            TextView urbanizacion = (TextView) myView.findViewById(R.id.urbanizacion);
            if (cursor.getString(3) != null)
                urbanizacion.setText("(" + cursor.getString(3) + ")");
            else
                urbanizacion.setText("");

            int maxNumSupplies = cursor.getInt(6);
            int suppliesRead = cursor.getInt(5);

            ProgressBar progressBar = (ProgressBar) myView.findViewById(R.id.progress_bar_street);

            progressBar.setMax(maxNumSupplies);
            progressBar.setProgress(suppliesRead);

            TextView completedStreet = (TextView) myView.findViewById(R.id.completed_street);
            completedStreet.setText(suppliesRead + "/" + maxNumSupplies);
        }

        return myView;
    }

    public void changeCursor(Cursor newCursor){

        //If the cursors are the same do nothing
        if(cursor == newCursor){
            return;
        }
        //Swap the cursors
        Cursor previous = cursor;
        cursor = newCursor;

        //Notify the adapter to update the new data
        if(cursor != null){
            this.notifyDataSetChanged();
        }

        //Close previous cursor
        if(previous != null){
            previous.close();
        }
    }



    /**
     *  Methods to implement filters
     **/

}
