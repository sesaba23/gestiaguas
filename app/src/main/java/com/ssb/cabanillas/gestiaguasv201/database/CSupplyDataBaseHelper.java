package com.ssb.cabanillas.gestiaguasv201.database;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


import com.ssb.cabanillas.gestiaguasv201.R;
import com.ssb.cabanillas.gestiaguasv201.database.BDGestiaguasDAO;
import com.ssb.cabanillas.gestiaguasv201.database.BDMicrosoftAccessDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sergio on 24/01/16.
 */
public class CSupplyDataBaseHelper extends SQLiteOpenHelper {

    private ResultSet cdr; // Reference to cursor to store queries
    private ProgressDialog progressDialog;
    private Context context;
    /**
     * DATABASE VERSION
     */
    private static final int DB_VERSION = 1;

    /**
     * TABLE STRINGS
     */
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DATE_TYPE = " DATETIME";
    private static final String COMMA = ", ";

    /**
     * SQL CREATE TABLE STREETS sentence
     */
    private static final String CREATE_STREETS_TABLE =
              "CREATE TABLE " + DatabaseContract.Streets.TABLE_NAME + "("
            + DatabaseContract.Streets._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT, "
            + DatabaseContract.Streets.ABREV_TIPO_VIA + TEXT_TYPE + COMMA
            + DatabaseContract.Streets.NOMBRE_CALLE + TEXT_TYPE + COMMA
            + DatabaseContract.Streets.URBANIZACION + TEXT_TYPE + COMMA
            + DatabaseContract.Streets.COMPLETED + INTEGER_TYPE + COMMA
            + DatabaseContract.Streets.NUM_COMPLETED + INTEGER_TYPE + COMMA
            + DatabaseContract.Streets.NUM_SUPPLIES + INTEGER_TYPE + ");";

    /**
     * SQL CREATE TABLE SUPPLIES sentence
     */
    private static final String CREATE_SUPPLIES_TABLE =
            "CREATE TABLE " + DatabaseContract.Supplies.TABLE_NAME + "("
                    + DatabaseContract.Supplies._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT, "
                    + DatabaseContract.Supplies.ID + INTEGER_TYPE + COMMA
                    + DatabaseContract.Supplies.ABREV_TIPO_VIA + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.NOMBRE_CALLE + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.NUM + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.LOCAL + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.PORTAL + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.BLOQUE + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.ESCALERA + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.PLANTA + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.LETRA + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.UWPAR + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.NUM_CONTADOR + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.CALIBRE_MM + INTEGER_TYPE + COMMA
                    + DatabaseContract.Supplies.UWLANT + INTEGER_TYPE + COMMA
                    + DatabaseContract.Supplies.UWFACT + DATE_TYPE + COMMA
                    + DatabaseContract.Supplies.UWLACT + INTEGER_TYPE + COMMA
                    + DatabaseContract.Supplies.CONSUMO + INTEGER_TYPE + COMMA
                    + DatabaseContract.Supplies.ABREV_ESTADO + TEXT_TYPE + COMMA
                    + DatabaseContract.Supplies.ESTADOS + TEXT_TYPE + ");";

    /**
     * SQL CREATE TABLE STATES sentence
     */
    private static final String CREATE_STATES_TABLE =
            "CREATE TABLE " + DatabaseContract.States.TABLE_NAME + "("
                    + DatabaseContract.States._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT, "
                    + DatabaseContract.States.ABREV_ESTADO + TEXT_TYPE + COMMA
                    + DatabaseContract.States.ESTADOS + TEXT_TYPE + ");";

    //Constructor
    public CSupplyDataBaseHelper(Context context){
        super(context, DatabaseContract.DB_NAME, null, DB_VERSION); //Null is a advanced feature relating cursors
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        /* Creates supplies table*/
        db.execSQL(CREATE_SUPPLIES_TABLE);
        /* Creates streets table */
        db.execSQL(CREATE_STREETS_TABLE);
        /* Creates estate table */
        db.execSQL(CREATE_STATES_TABLE);
        // Make work in background thread
        new ConectionMySQLTask().execute(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }

    /* This method helps to introduce data registers in the data Base's supply table*/
    private static void insertSupply(SQLiteDatabase db, int ID, String abrevTipoVia, String calle, String num, String local,
                                     String portal, String  bloque, String escalera, String planta,
                                     String letra, String uwpar, String numContador, int calibreMm, int uwlant,
                                     Date uwfact, int uwlact, int consumo, String abrevEstado, String estados){

        /*Helps to insert date format in "uwfact" field*/
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        //Object to create pairs of name-values
        ContentValues supplyValues = new ContentValues();
        supplyValues.put(DatabaseContract.Supplies.ID, ID);
        supplyValues.put(DatabaseContract.Supplies.ABREV_TIPO_VIA, abrevTipoVia);
        supplyValues.put(DatabaseContract.Supplies.NOMBRE_CALLE, calle);
        supplyValues.put(DatabaseContract.Supplies.NUM, num);
        supplyValues.put(DatabaseContract.Supplies.LOCAL, local);
        supplyValues.put(DatabaseContract.Supplies.PORTAL, portal);
        supplyValues.put(DatabaseContract.Supplies.BLOQUE, bloque);
        supplyValues.put(DatabaseContract.Supplies.ESCALERA, escalera);
        supplyValues.put(DatabaseContract.Supplies.PLANTA, planta);
        supplyValues.put(DatabaseContract.Supplies.LETRA, letra);
        supplyValues.put(DatabaseContract.Supplies.UWPAR, uwpar);
        supplyValues.put(DatabaseContract.Supplies.NUM_CONTADOR, numContador);
        supplyValues.put(DatabaseContract.Supplies.CALIBRE_MM, calibreMm);
        supplyValues.put(DatabaseContract.Supplies.UWLANT, uwlant);
        /* There is no need to complete date field. First of all it should be null because no read has benn done */
//        supplyValues.put("UWFACT", dateFormat.format(uwfact));
        supplyValues.put(DatabaseContract.Supplies.UWLACT, uwlact);
        supplyValues.put(DatabaseContract.Supplies.CONSUMO, consumo);
        supplyValues.put(DatabaseContract.Supplies.ABREV_ESTADO, abrevEstado);
        supplyValues.put(DatabaseContract.Supplies.ESTADOS, estados);

        db.insert(DatabaseContract.Supplies.TABLE_NAME, null, supplyValues);
    }

    /* This method helps to introduce data registers in the data Base's streets table*/
    private void insertStreet(SQLiteDatabase db, String abrevTipoVia, String nombreCalle, String urbanizacion, int num_suministros){

        //Object to create pairs of name-values
        ContentValues supplyValues = new ContentValues();

        supplyValues.put(DatabaseContract.Streets.ABREV_TIPO_VIA, abrevTipoVia);
        supplyValues.put(DatabaseContract.Streets.NOMBRE_CALLE, nombreCalle);
        supplyValues.put(DatabaseContract.Streets.URBANIZACION, urbanizacion);
        supplyValues.put(DatabaseContract.Streets.NUM_SUPPLIES, num_suministros);
        supplyValues.put(DatabaseContract.Streets.COMPLETED, 0);
        supplyValues.put(DatabaseContract.Streets.NUM_COMPLETED, 0);

        db.insert(DatabaseContract.Streets.TABLE_NAME, null, supplyValues);
    }

    /* This method helps to introduce data registers in the data Base's streets table*/
    private static void insertStates(SQLiteDatabase db, String abrevEstado, String estados){
        //Object to create pairs of name-values
        ContentValues supplyValues = new ContentValues();

        supplyValues.put(DatabaseContract.States.ABREV_ESTADO, abrevEstado);
        supplyValues.put(DatabaseContract.States.ESTADOS, estados);

        db.insert(DatabaseContract.States.TABLE_NAME, null, supplyValues);
    }

    private class ConectionMySQLTask extends AsyncTask<SQLiteDatabase, Void, ResultSet> {

        @Override
        protected ResultSet doInBackground(SQLiteDatabase... db){

            int count = 0;
            int num_suministros = 0;
            /* We read data base type preferences*/
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String dataBaseType = sharedPreferences.getString("preferences_data_base", "Access");
            BDGestiaguasDAO BD;
            BDMicrosoftAccessDAO BDAccess = null;

            try {
                BD = new BDGestiaguasDAO();
                BDAccess = new BDMicrosoftAccessDAO();

                //Choose the database which has been selected in settings activity
                if(dataBaseType.contains("MySQL")) {
                    cdr = BD.obtenerListadoDeCalles();
                }else{ //If Access has been selected...
                    cdr = BDAccess.obtenerListadoDeCallesAgrupadas();
                }
                /* Look in the database each street and store locally in SQLite database 'STREETS' table*/
                cdr.beforeFirst();
                while(cdr.next())
                {
                    insertStreet(db[0], cdr.getString(DatabaseContract.Streets.ABREV_TIPO_VIA),
                            cdr.getString(DatabaseContract.Streets.NOMBRE_CALLE),
                            cdr.getString(DatabaseContract.Streets.URBANIZACION),
                            cdr.getInt(DatabaseContract.Streets.NUM_SUPPLIES));
                    count++;
                }
                Log.d("CSupplyDataBaseHelper", "Se han creado: " + count + " calles");
                count=0;


                if(dataBaseType.contains("MySQL")) {
                    cdr= BD.obtenerSuministros();
                }else{
                    cdr = BDAccess.obtenerSuministros();
                }
                /* Look in the database each supply and store locally in SQLite database 'SUPPLIES' table*/
                cdr.beforeFirst();
                while(cdr.next() && count < 10000)
                {
                    int auxNum;
                    //Aux. variable to calculate if the supply is in an odd or even number of the street
                    try{
                        auxNum = Integer.parseInt(cdr.getString(DatabaseContract.Supplies.NUM));
                    }catch(NumberFormatException num){
                        /* If the value stored in the field is not a number we treat like a zero*/
                        auxNum = 0;
                    }

                    String auxuwpar = isNumberEvenOrOddorPol(auxNum, cdr.getString(DatabaseContract.Supplies.ABREV_TIPO_VIA));

                    insertSupply(db[0], cdr.getInt(DatabaseContract.Supplies.ID),
                            cdr.getString(DatabaseContract.Supplies.ABREV_TIPO_VIA),
                            cdr.getString(DatabaseContract.Supplies.NOMBRE_CALLE),
                            cdr.getString(DatabaseContract.Supplies.NUM),
                            cdr.getString(DatabaseContract.Supplies.LOCAL),
                            cdr.getString(DatabaseContract.Supplies.PORTAL),
                            cdr.getString(DatabaseContract.Supplies.BLOQUE),
                            cdr.getString(DatabaseContract.Supplies.ESCALERA),
                            cdr.getString(DatabaseContract.Supplies.PLANTA),
                            cdr.getString(DatabaseContract.Supplies.LETRA), auxuwpar,
                            cdr.getString(DatabaseContract.Supplies.NUM_CONTADOR),
                            cdr.getInt(DatabaseContract.Supplies.CALIBRE_MM),
                            cdr.getInt(DatabaseContract.Supplies.UWLANT), new Date(),
                            cdr.getInt(DatabaseContract.Supplies.UWLACT),
                            cdr.getInt(DatabaseContract.Supplies.CONSUMO),
                            cdr.getString(DatabaseContract.Supplies.ABREV_ESTADO),
                            cdr.getString(DatabaseContract.Supplies.ESTADOS));
                    count++;
                }
                Log.d("CSupplyDataBaseHelper", "Se han creado: " + count + " suministros");
                count = 0;

                if(dataBaseType.contains("MySQL")) {
                    cdr= BD.obtenerEstados();
                }else{
                    cdr = BDAccess.obtenerEstados();
                }
                /* Look in the database each state and store locally in SQLite database 'STATES' table*/
                cdr.beforeFirst();
                while(cdr.next())
                {
                    insertStates(db[0], cdr.getString(DatabaseContract.States.ABREV_ESTADO), cdr.getString(DatabaseContract.States.ESTADOS));
                    count++;
                }
                Log.d("CSupplyDataBaseHelper", "Se han creado: " + count + " estados");
            } catch (ClassNotFoundException | SQLException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            } finally {
                //Close connection
                if (BDAccess != null) {
                    try { BDAccess.cerrarConexion();
                    } catch (SQLException e) {e.printStackTrace();}
                }
            }

            return cdr;
        }

        @Override
        protected void onPreExecute() {
            /* With the last two arguments the progress bar is indeterminate and you can not cancel the execution*/
            progressDialog = ProgressDialog.show(context,
                    context.getResources().getString(R.string.title_downloading_database_dialog),
                    context.getResources().getString(R.string.message_update_database_dialog),
                    true, false);
        }

        @Override
        protected void onPostExecute(ResultSet resultSet) {
            progressDialog.dismiss();
            Toast.makeText(context, R.string.finish_downloading_database, Toast.LENGTH_LONG).show();

        }
    }

    /* Indicates if the argument is an even or an odd number or a Industrial Street*/
    private String isNumberEvenOrOddorPol(int auxNum, String abreTipoVia){

        if(abreTipoVia.contains(DatabaseContract.PG))
            return DatabaseContract.PG;
        else {
            if ((auxNum % 2) == 0)
                return DatabaseContract.PAR;
            else
                return DatabaseContract.IMPAR;
        }
    }
}
