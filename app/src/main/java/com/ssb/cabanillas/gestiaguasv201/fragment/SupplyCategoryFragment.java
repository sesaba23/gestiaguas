package com.ssb.cabanillas.gestiaguasv201.fragment;


import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.ssb.cabanillas.gestiaguasv201.adapter.SupplyListAdapter;
import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupplyCategoryFragment extends ListFragment {


    private SQLiteDatabase db; //Reference to the data base
    private Cursor cdr; // Reference to resultset to store queries

    private String selectedStreet;
    private ArrayList<Integer> supplyNoList = new ArrayList<>();
    /*We store if we want to show odd or even supplies*/
    private Boolean[] selectedNumbers = new Boolean[2];
    private String showNumbersOdd, showNumbersEven;

    private Context context;
    private int positionList; //Save the current position of the listview when get out to another activityw
    private int topPosition;

    // We add the listener to the fragment
    public interface supplyListListener {
        void itemClicked(long id);
    }
    private supplyListListener listener;

    public SupplyCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        selectedStreet = (String) getActivity().getIntent().getExtras().get(DatabaseContract.SELECTEDSTREET);
        System.out.println("selectedStreet: " + selectedStreet);

        /* Read preferences setting in order to show even or odd numbers*/
        getPreferences();
        context = inflater.getContext();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        try{
            SQLiteOpenHelper supplyDataBaseHelper = new CSupplyDataBaseHelper(getActivity());
            db = supplyDataBaseHelper.getReadableDatabase(); // Get the reference to a data base

            System.out.println("Entra en ONStart");

            /* Show only the supplies from the selected street*/
            cdr = db.query("SUPPLIES", new String[]{"_id", "ABREV_TIPO_VIA", "NOMBRE_CALLE", "NUM", "LOCAL", "PORTAL",
                            "BLOQUE", "ESCALERA", "PLANTA", "LETRA", "UWPAR", "UWFACT", "CONSUMO", "NUM_CONTADOR", "CONSUMO"},
                    "NOMBRE_CALLE = ? and (UWPAR = ? or UWPAR = ?)",
                    new String[]{selectedStreet, showNumbersOdd, showNumbersEven},
                    null, null, null);

            //Reset the arrayList of supplies numbers
            supplyNoList.clear();
            /* Get the list of supplies' id of the street*/
            while (cdr.moveToNext()){
                supplyNoList.add(cdr.getInt(0));
            }
            SupplyListAdapter listAdapter = new SupplyListAdapter(getActivity(), cdr);
            setListAdapter(listAdapter);

        }catch (SQLiteException e){
            Toast toast = Toast.makeText(getActivity(), "Data Base unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    /* Close de Cursor and de dataBAse when exit the activity*/
    @Override
    public void onDestroy(){
        super.onDestroy();
        cdr.close();
        db.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        //When come back of supply activity place the selected supply at the top view
        getListView().setSelectionFromTop(positionList, 0);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if(listener != null) {
            positionList = position;
//            positionList = getListView().getFirstVisiblePosition();
//            topPosition = (v == null) ? 0 : v.getTop();
            listener.itemClicked(id);
        }
    }

    //This is called when the fragment gets attached to the activity
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        this.listener = (supplyListListener)activity;
    }

    public void getPreferences(){
         /* Read shared preferences*/
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        selectedNumbers[0] = preferences.getBoolean("preferences_odd", true);
        selectedNumbers[1] = preferences.getBoolean("preferences_even", true);
        if(selectedNumbers[0]) {
            showNumbersOdd = "PAR";
        }else {
            showNumbersOdd = "";
        }
        if(selectedNumbers[1]) {
            showNumbersEven = "IMPAR";
        }else {
            showNumbersEven = "";
        }
    }


    public ArrayList<Integer> getSupplyNoList() {
        return supplyNoList;
    }
}
