package com.ssb.cabanillas.gestiaguasv201.database;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.sql.SQLException;

/**
 * Created by sergio on 6/03/16.
 */
public class CSupplyDataBaseSQLUpdate extends AsyncTask<Cursor, Void, Cursor> {

    private Context context;
    private ProgressDialog progressDialog;

    private Cursor cdr; // Reference to resultset to store queries

    private int updatedRegisters; //Stores the number of supplies that have already been updated

    //Constructor
    public CSupplyDataBaseSQLUpdate(Context context){
        this.context = context;
    }

    public void actualizarDatosBDSQL(Cursor cdr){
        this.cdr = cdr;

        while(cdr.moveToNext()){
            System.out.println("IDs pasados al hilo: " + cdr.getInt(0));
        }
        cdr.moveToFirst();cdr.moveToPrevious();

        // Make work in background thread
        new CSupplyDataBaseSQLUpdate(context).execute(cdr);
    }

    @Override
    protected Cursor doInBackground(Cursor... params){

        int count = 0;
            /* We read data base type preferences*/
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String dataBaseType = sharedPreferences.getString("preferences_data_base", "Access");
        BDGestiaguasDAO BD;
        BDMicrosoftAccessDAO BDAccess;

        try {
            BD = new BDGestiaguasDAO();
            BDAccess = new BDMicrosoftAccessDAO();

            if(dataBaseType.contains("MySQL")) {
                // Pending of complete coding when programing with JAVA aplication
            }else{

                updatedRegisters = BDAccess.actualizarSuministrosLeidos(params[0]);
                System.out.println("Se han actualizado " + updatedRegisters + " suministros");
            }


        } catch (ClassNotFoundException | SQLException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        return cdr;
    }

    @Override
    protected void onPreExecute() {

        String title = "Actualizando Base de Datos...";
        String dialog = "Este proceso puede tardar unos segundos. \nPor favor, espere.";
            /* With the last two arguments the progress bar is indeterminate and you can not cancel the execution*/
        progressDialog = ProgressDialog.show(context, title,
                dialog, true, false);
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        progressDialog.dismiss();
        //finish_update_database
        Toast.makeText(context, "Se han actualizado correctamente " + Integer.toString(updatedRegisters) + " suministros", Toast.LENGTH_LONG).show();
    }
}

