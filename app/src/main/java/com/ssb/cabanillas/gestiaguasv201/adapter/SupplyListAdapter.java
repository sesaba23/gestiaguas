package com.ssb.cabanillas.gestiaguasv201.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ssb.cabanillas.gestiaguasv201.R;


/**
 * Created by sergio on 1/02/16.
 */
public class SupplyListAdapter extends BaseAdapter {

    //protected Activity activity;
    protected Context context;
    protected Cursor cursor;

    public SupplyListAdapter(Context context, Cursor cursor){
        this.context = context;
        this.cursor = cursor;
    }

    @Override
    public int getCount() {
        return cursor.getCount();
    }

    @Override
    public Object getItem(int position) {
        cursor.moveToPosition(position);
        return cursor.getPosition();
    }

    @Override
    public long getItemId(int position) {
        cursor.moveToPosition(position);
        return cursor.getInt(0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View myview;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            myview = inflater.inflate(R.layout.item_listview_supply, null);
        }else {
            myview = convertView;
        }

        cursor.moveToPosition((int)getItem(position));

        TextView numCalle = (TextView) myview.findViewById(R.id.numero);
        numCalle.setText(cursor.getString(3));

        TextView local = (TextView) myview.findViewById(R.id.local);
        local.setText(cursor.getString(4));

        TextView portal = (TextView) myview.findViewById(R.id.portal);
        portal.setText(cursor.getString(5));

        TextView bloque = (TextView) myview.findViewById(R.id.bloque);
        bloque.setText(cursor.getString(6));

        TextView escalera = (TextView) myview.findViewById(R.id.escalera);
        escalera.setText(cursor.getString(7));

        TextView planta = (TextView) myview.findViewById(R.id.planta);
        planta.setText(cursor.getString(8));

        TextView letra = (TextView) myview.findViewById(R.id.letra);
        letra.setText(cursor.getString(9));

        String completed = cursor.getString(11);
        /* If the supply has been read (so the UWFACT is not null) change background color*/
        if(completed != null) {
            myview.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryCompleted));

        }else{
            myview.setBackgroundColor(Color.TRANSPARENT);
        }


        return myview;
    }
}
