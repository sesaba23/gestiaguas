package com.ssb.cabanillas.gestiaguasv201.backup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
// Google DRIVE API libraries
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;

/**
 * Created by sergio on 5/02/16.
 *
 * Help to make a data backup
 */
public class CBackUpDataBase extends Activity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "BaseDriveActivity";
    /* Request code for auto Google Play Services error resolution. */
    protected static final int REQUEST_CODE_RESOLUTION = 1;
    /* Google API client. */
    private GoogleApiClient mGoogleApiClient;

    /* DriveId of an existing folder to be used as a parent folder */
    private DriveId mFolderDriveId;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String title = "Respaldando Base de Datos...";
        String dialog = "Este proceso puede tardar unos segundos. \nPor favor, espere.";
        /* With the last two arguments the progress bar is indeterminate and you can not cancel the execution*/
        progressDialog = ProgressDialog.show(this, title, dialog, true, false);
    }

    /**
     * Called when activity gets visible. A connection to Drive services need to
     * be initiated as soon as the activity is visible. Registers
     * {@code ConnectionCallbacks} and {@code OnConnectionFailedListener} on the
     * activities itself.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER) // required for App Folder sample
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    /**
     * Called when activity gets invisible. Connection to Drive service needs to
     * be disconnected as soon as an activity is invisible.
     */
    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }


    public static void createDataBaseBackUp() throws IOException{

        String directory = "/storage/emulated/0/Gestiaguas";
        File d = new File(directory);
        if(!d.exists()){
            d.mkdir();
            System.out.println("Directorio Creado");
        }

        final String inFileName = "/data/data/com.ssb.cabanillas.gestiaguasv201/databases/" + DatabaseContract.DB_NAME;
        File dbFile = new File(inFileName);

        FileInputStream fileInputStream = new FileInputStream(dbFile);

        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        String outputFileName = directory + "/" + DatabaseContract.DB_NAME + "_" + timeStamp;

        OutputStream output = new FileOutputStream(outputFileName);

        byte[] buffer = new byte[1024];
        int length;
        while((length = fileInputStream.read(buffer)) > 0){
            output.write(buffer, 0, length);
        }

        output.flush();
        output.close();
        fileInputStream.close();
    }

    /**
     * Handles resolution callbacks.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_RESOLUTION && resultCode == RESULT_OK) {
            mGoogleApiClient.connect();
        }
    }

    /* Called when {@code mGoogleApiClient} is connected. */
    public void onConnected(Bundle connectionHint) {


        // Query if the folder already exits
        Query query = new Query.Builder()
                .addFilter(Filters.and(Filters.eq(
                                SearchableField.TITLE, "Gestiaguas_Cabanillas"),
                        Filters.eq(SearchableField.TRASHED, false)))
                .build();
        Drive.DriveApi.query(getGoogleApiClient(), query)
                .setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                    @Override
                    public void onResult(DriveApi.MetadataBufferResult result) {
                        if (!result.getStatus().isSuccess()) {
                            showMessage("Cannot create folder in the root.");
                        } else {
                            boolean isFound = false;
                            for (Metadata m : result.getMetadataBuffer()) {
                                if (m.getTitle().equals("Gestiaguas_Cabanillas")) {
//                                    showMessage("Folder exists");
                                    isFound = true;
                                    //Gets the folder Id in order to copy data Base inside
                                    mFolderDriveId = m.getDriveId();
                                    break;
                                }
                            }
                            if (!isFound) {
//                                showMessage("Folder not found; creating it.");
                                //Create a folder in google drive if it doesn't exits
                                MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                        .setTitle("Gestiaguas_Cabanillas").build();
                                Drive.DriveApi.getRootFolder(getGoogleApiClient()).createFolder(
                                        getGoogleApiClient(), changeSet).setResultCallback(callback);
                            }
                            //Release the metaDataBuffer
                            result.getMetadataBuffer().release();
                            //And create File throw a callback function
                            Drive.DriveApi.newDriveContents(getGoogleApiClient())
                                    .setResultCallback(driveContentsCallback);
                            Log.i(TAG, "GoogleApiClient connected");
                        }
                    }
                });
    }

    /* Called when {@code mGoogleApiClient} is disconnected. */
                        @Override
                        public void onConnectionSuspended ( int i){}

                        /* Called when {@code mGoogleApiClient} is trying to connect but failed.
                         * Handle {@code result.getResolution()} if there is a resolution is
                         * available. */
                        @Override
                        public void onConnectionFailed (ConnectionResult result) {
                            Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
                            if (!result.hasResolution()) {
                                // show the localized error dialog.
                                GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
                                return;
                            }
                            try {
                                result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
                            } catch (IntentSender.SendIntentException e) {
                                Log.e(TAG, "Exception while starting resolution activity", e);
                            }
                        }

    /* Getter for the {@code GoogleApiClient}. */

                    public GoogleApiClient getGoogleApiClient() {
                        return mGoogleApiClient;
                    }


                    final ResultCallback<DriveFolderResult> callback = new ResultCallback<DriveFolderResult>() {
                        @Override
                        public void onResult(DriveFolderResult result) {

                            mFolderDriveId = result.getDriveFolder().getDriveId();
                            if (!result.getStatus().isSuccess()) {
                                showMessage("Error while trying to create the folder");
                                return;
                            }
                            showMessage("Created folder: " + mFolderDriveId);
                        }
                    };

                    final private ResultCallback<DriveApi.DriveContentsResult> driveContentsCallback = new
                            ResultCallback<DriveApi.DriveContentsResult>() {
                                @Override
                                public void onResult(DriveApi.DriveContentsResult result) {
                                    if (!result.getStatus().isSuccess()) {
                                        showMessage("Error while trying to create new file contents");
                                        return;
                                    }
                                    final DriveContents driveContents = result.getDriveContents();

                                    // Perform I/O off the UI thread.
                                    new Thread() {
                                        @Override
                                        public void run() {
                                            // write content to DriveContents
                                            OutputStream outputStream = driveContents.getOutputStream();

                                            try {
                                                final String inFileName = "/data/data/com.ssb.cabanillas.gestiaguasv201/databases/"
                                                        + DatabaseContract.DB_NAME;
                                                File dbFile = new File(inFileName);

                                                FileInputStream fileInputStream = new FileInputStream(dbFile);
                                                byte[] buffer = new byte[1024];
                                                int length;
                                                while ((length = fileInputStream.read(buffer)) > 0) {
                                                    outputStream.write(buffer, 0, length);
                                                }
                                                outputStream.flush();
                                                outputStream.close();
                                                fileInputStream.close();
                                            } catch (IOException e) {
                                                Log.e(TAG, e.getMessage());
                                            }

                                            String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
                                            MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                                    .setTitle("BackUp_" + DatabaseContract.DB_NAME + "_" + timeStamp)
                                                    .setMimeType("text/plain")
                                                    .setStarred(true).build();

                                            DriveFolder folder = mFolderDriveId.asDriveFolder();
                                            folder.createFile(getGoogleApiClient(), changeSet, driveContents)
                                                    .setResultCallback(fileCallback);
//                            // create a file on root folder
//                            Drive.DriveApi.getRootFolder(getGoogleApiClient())
//                                    .createFile(getGoogleApiClient(), changeSet, driveContents)
//                                    .setResultCallback(fileCallback);
                                        }
                                    }.start();
                                }
                            };

                    final private ResultCallback<DriveFolder.DriveFileResult> fileCallback = new
                            ResultCallback<DriveFolder.DriveFileResult>() {
                                @Override
                                public void onResult(DriveFolder.DriveFileResult result) {
                                    if (!result.getStatus().isSuccess()) {
                                        showMessage("Error while trying to create the file");
                                        return;
                                    }
                                    progressDialog.dismiss();
                                    showMessage("Archivo respaldado CORRECTAMENTE en Google Drive");
                                    //FinishActivity
                                    finish();
                                }
                            };

                    /**
                     * Shows a toast message.
                     */
                    public void showMessage(String message) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                }
