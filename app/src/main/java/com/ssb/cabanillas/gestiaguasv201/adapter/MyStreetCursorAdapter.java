package com.ssb.cabanillas.gestiaguasv201.adapter;

import android.animation.ObjectAnimator;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.ssb.cabanillas.gestiaguasv201.R;
import com.ssb.cabanillas.gestiaguasv201.SupplyCategoryActivity;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;
import com.ssb.cabanillas.gestiaguasv201.database.GAProvider;

/**
 * Created by sergio on 19/04/16.
 */
public class MyStreetCursorAdapter extends CursorAdapter
        implements AdapterView.OnItemClickListener, FilterQueryProvider, AdapterView.OnItemLongClickListener {

    private Context mContext;
    private Cursor cursor;

    private ListView lv;

    //Necessary to access street table
    private final ContentResolver cr;
    private final Uri uriPath;
    private String streetsToShow;

    private int positionList; //Save the current position of the listview when get out to another activity

    public MyStreetCursorAdapter(Context context, Cursor c, int autoRequery, String streetsToShow) {
        super(context, c, autoRequery);
        this.mContext = context;
        this.cursor = c;
        this.streetsToShow = streetsToShow;

        //Initialize content resolver and uri in order to access content provider
        cr = mContext.getContentResolver();
        uriPath = Uri.parse(GAProvider.STREETS_TABLE_URI);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View myView = LayoutInflater.from(context).inflate(R.layout.item_listview_street, null);
        //Set the onItemClicked Listener to the listview
        lv = (ListView) parent.findViewById(R.id.street_list);
        lv.setOnItemClickListener(this);
        lv.setOnItemLongClickListener(this);

        return myView;

    }

    @Override
    public void bindView(View myView, Context context, Cursor cursor) {
        if (cursor != null) {
            TextView checkedPressed = (TextView) myView.findViewById(R.id.completed_state);
            checkedPressed.setTag(cursor.getString(2)); //Save the name of the street so we can know where street has been clicked

            TextView abrevTipoVia = (TextView) myView.findViewById(R.id.abrevtipovia);
            abrevTipoVia.setText(cursor.getString(1));

            TextView calle = (TextView) myView.findViewById(R.id.calle);
            calle.setText(cursor.getString(2));

            TextView urbanizacion = (TextView) myView.findViewById(R.id.urbanizacion);
            if (cursor.getString(3) != null)
                urbanizacion.setText("(" + cursor.getString(3) + ")");
            else
                urbanizacion.setText("");

            //Set and populate the progress bar and extra information
            int maxNumSupplies = cursor.getInt(6);
            int suppliesRead = cursor.getInt(5);
            ProgressBar progressBar = (ProgressBar) myView.findViewById(R.id.progress_bar_street);
            progressBar.setMax(maxNumSupplies);
            progressBar.setProgress(suppliesRead);
            TextView completedStreet = (TextView) myView.findViewById(R.id.completed_street);
            completedStreet.setText(suppliesRead + "/" + maxNumSupplies);

            /* Show the status of the street*/
            TextView completed = (TextView) myView.findViewById(R.id.completed_state);
            if (cursor.getString(4).contains("0")) {
                completed.setBackground(mContext.getResources().getDrawable(R.drawable.edittextwithborderred));
                progressBar.getProgressDrawable().setColorFilter(mContext.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            }
            else {
                completed.setBackground(mContext.getResources().getDrawable(R.drawable.edittextwithborder));
                progressBar.getProgressDrawable().setColorFilter(mContext.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d("StreetAdapter", "Street Selected in adapter with id: " + id);

        Uri uriPath = Uri.parse(GAProvider.STREETS_SELECTED_URI + id);
        Cursor cdr = cr.query(uriPath, null, null, null, null);
        try{
            if(cdr.moveToFirst()) {
                String abrevTipoVia = cdr.getString(1);
                String selectedStreet = cdr.getString(2);

                /* Open the supplyCategoryActivity and stored the clicked street*/
                Intent intent = new Intent(mContext, SupplyCategoryActivity.class);
                intent.putExtra(DatabaseContract.SELECTEDABREVTIPOVIA, abrevTipoVia);
                intent.putExtra(DatabaseContract.SELECTEDSTREET, selectedStreet);
                mContext.startActivity(intent);
            }
            else{
                Toast.makeText(mContext, "La calle seleccionada, actualmente no tiene suministros" +
                        " dados de alta", Toast.LENGTH_SHORT).show();
            }
            cdr.close();
        }catch (SQLiteException e) {
                /* If access to the data base fails show a message*/
            Toast toast = Toast.makeText(mContext, "Data Base unavailable", Toast.LENGTH_SHORT);
            toast.show();
            cdr.close();
        }
        positionList = lv.getFirstVisiblePosition(); //Set the current position of the listview
    }

    public int getPositionList() {
        return positionList;
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("StreetAdapter", "On long click in the street: " + id);

        TextView completed = (TextView) view.findViewById(R.id.completed_state);

        ContentValues updateNumCompleted = new ContentValues();

        Uri uriPathId = Uri.parse(GAProvider.STREETS_SELECTED_URI + id);
        Cursor cdr = cr.query(uriPathId, null, null, null, null);

        cdr.moveToFirst();
        if (cdr.getString(4).contains("0")) {
            Log.d("StreetAdapter", "Calle a marcar como completada: " + cdr.getString(2));
            //Update the "NUM_COMPLETED" field with the number of supplies already completed in the street
            updateNumCompleted.put(DatabaseContract.Streets.COMPLETED, 1);
            Toast.makeText(mContext, cdr.getString(1) + " " + cdr.getString(2) + " marcada como COMPLETADA", Toast.LENGTH_SHORT).show();
        }else{
            Log.d("StreetAdapter", "Calle a marcar como pendiente: " + cdr.getString(2));
            //Update the "NUM_COMPLETED" field with the number of supplies already completed in the street
            updateNumCompleted.put(DatabaseContract.Streets.COMPLETED, 0);
            Toast.makeText(mContext, cdr.getString(1) + " " + cdr.getString(2) +" marcada como PENDIENTE", Toast.LENGTH_SHORT).show();
        }
        // Set Animations
        Animation shake = AnimationUtils.loadAnimation(mContext, R.anim.shakeanimation);
//        completed.setAnimation(shake);
        completed.startAnimation(shake);

        //Use of content provider in order to update STREETS database.
        cr.update(uriPath, updateNumCompleted, null, new String[]{cdr.getString(2)});
        notifyDataSetChanged();

        cdr.close();
        return true;
    }

    /**
     * NOT USE when we are implementing CONTENT PROVIDERS and CURSOR LOADER
     * Example of use a filter when we are using cursor adapter with listview implementing a simple cursor
     * @param constraint
     * @return
     */

    @Override
    public Cursor runQuery(CharSequence constraint) {
        String strItemCode;
        if(constraint != null)
            strItemCode= constraint.toString();
        else
            strItemCode = "";

        Uri uriPath = Uri.parse(GAProvider.STREETS_TABLE_URI);
        return cr.query(uriPath,                                                //Content Provider Table
                null,                                                           //Projection
                "(" + DatabaseContract.Streets.NOMBRE_CALLE + " LIKE ? "              //Selection
                        + "OR " + DatabaseContract.Streets.ABREV_TIPO_VIA + " LIKE ? "
                        + "OR " + DatabaseContract.Streets.URBANIZACION + " LIKE ? ) "
                        + "AND " + DatabaseContract.Streets.COMPLETED + " LIKE ?",
                new String[] {strItemCode + "%", strItemCode + "%", strItemCode + "%", streetsToShow},//Selection Arguments
                null);
    }
}
