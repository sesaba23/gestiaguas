package com.ssb.cabanillas.gestiaguasv201.fragment;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.ssb.cabanillas.gestiaguasv201.R;
import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class SupplyFragment extends Fragment implements View.OnFocusChangeListener{

    private int supplyNo;
    private int oldUWlact; //Store the previos uwlact shown
    private int uwlact, uwlant; //Store the lectures supply shown
    private String estado, oldEstado; //Store the state of the supply
    private View viewPhone; //Store if we are on a phone

    //Use in intents to launch camera and e-mail
    public static final int REQUEST_CODE_FROM_CAMERA = 112;
    private Uri fileUri;
    private String imagePath = "";

    //Auxiliary variable to temporaly store information about supply selected
    private String auxAbrevTipoVia, auxCalle, auxNum, auxNumContador, auxLocal,
            auxPortal, auxBloque, auxEscalera, auxPlanta, auxLetra, auxEstado,
            auxCalibre, auxUWfact;
    private int auxUWlant, auxUWlact, auxConsumo;
    private String textMessage, subjectMessage;

    private Activity activity;

    /* Interface to comunicate with parent Activity*/
    public interface OnChangeSupplySelectedListener{
        void onChangeSupply(int uwlact, int uwlant, String estado, int supplyNoToSave);
    }

    public SupplyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /* We can not use getView() because View has not been created yet by onCreateView
        * Instead, we ask for the inflater view*/
        final View rootView = inflater.inflate(R.layout.fragment_supply, container, false);
        viewPhone = rootView.findViewById(R.id.numero);

        /* Set listener to the wlact field*/
        rootView.findViewById(R.id.uwlact).setOnFocusChangeListener(this);

        /*If you press back key treat as a focus change event in order to store the lecture if it has been modified*/
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    EditText uwlact = (EditText) rootView.findViewById(R.id.uwlact);
                    //Jump to the onFocusChange event
                    onFocusChange(uwlact, false);
                    return true;
                }
                return false;
            }
        });
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onResume(){
        super.onResume();

        View view = getView();
        if(view != null){
            try{
            /* Creating an reference to the dataBase*/
                SQLiteOpenHelper SupplyDataBaseHelper = new CSupplyDataBaseHelper(getActivity());
                SQLiteDatabase db = SupplyDataBaseHelper.getWritableDatabase();

            /* Create the resulset*/
                Cursor cdr = db.query("SUPPLIES", new String[]{"ABREV_TIPO_VIA", "NOMBRE_CALLE", "NUM", "LOCAL",
                                "PORTAL", "BLOQUE", "ESCALERA", "PLANTA", "LETRA", "ESTADOS", "NUM_CONTADOR",
                                "CALIBRE_MM", "UWLANT", "UWFACT", "UWLACT", "CONSUMO"},
                        "_id = ?",
                        new String[] { Integer.toString(supplyNo) },
                        null, null, null);

                //Move to the first record in the cursor
                if(cdr.moveToFirst()){

                    //Get supply details from the cursor
                    auxAbrevTipoVia = cdr.getString(0);
                    auxCalle = cdr.getString(1);
                    auxNum = cdr.getString(2);
                    auxLocal = cdr.getString(3);
                    auxPortal = cdr.getString(4);
                    auxBloque = cdr.getString(5);
                    auxEscalera = cdr.getString(6);
                    auxPlanta = cdr.getString(7);
                    auxLetra = cdr.getString(8);
                    auxEstado = cdr.getString(9);
                    auxNumContador = cdr.getString(10);
                    auxCalibre = cdr.getString(11);
                    auxUWlant = cdr.getInt(12);
                    auxUWfact = cdr.getString(13);
                    auxUWlact = cdr.getInt(14);
                    auxConsumo = cdr.getInt(15);

                    /* Set listener to floating action Button to shot photograph and send via e-mail*/
                    FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                                    .setAction("Action", null).show();

                            /*Open the mobile camera*/
                            fromCamera();
                        }
                    });

                    /* We only populate supply direction if we are on a phone. This is it, because in a tablet,
                    * the list of supply directions and supply details are shown together*/
                    if(viewPhone != null) {
                       getActivity().setTitle(auxAbrevTipoVia + "/ " + auxCalle);

                        //Populate the number of the street
                        TextView num = (TextView) view.findViewById(R.id.numero);
                        num.setText(auxNum);

                        //Populate de Local of the street
                        TextView local = (TextView) view.findViewById(R.id.local);
                        local.setText(auxLocal);

                        //Populate de Portal of the street
                        TextView portal = (TextView) view.findViewById(R.id.portal);
                        portal.setText(auxPortal);

                        //Populate de Bloque of the street
                        TextView bloque = (TextView) view.findViewById(R.id.bloque);
                        bloque.setText(auxBloque);

                        //Populate de Escalera of the street
                        TextView escalera = (TextView) view.findViewById(R.id.escalera);
                        escalera.setText(auxEscalera);

                        //Populate de Planta of the street
                        TextView planta = (TextView) view.findViewById(R.id.planta);
                        planta.setText(auxPlanta);

                        //Populate de Letra of the street
                        TextView letra = (TextView) view.findViewById(R.id.letra);
                        letra.setText(auxLetra);
                    }

                    /* Populate the rest of fields either if we are on a phone or a tablet*/
                    //Populate the counter number
                    TextView numContador = (TextView) view.findViewById(R.id.numero_contador);
                    numContador.setText(auxNumContador);

                    //Populate the dimensions of the supply
                    TextView calibre = (TextView) view.findViewById(R.id.calibre);
                    calibre.setText(auxCalibre + "mm");

                    //Set the spinner adapter and populate the state
                    Cursor cdrStates = db.query("STATE_SUPPLIES", new String[]{"_id", "ABREV_ESTADO", "ESTADOS"},
                            null, null,
                            null, null, null);
                    Spinner observaciones = (Spinner) view.findViewById(R.id.spinner_estado);
                    SpinnerAdapter spinnerAdapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_spinner_item, cdrStates,
                                      new String[]{"ESTADOS"},
                                      new int[]{android.R.id.text1},
                                      SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
                    observaciones.setAdapter(spinnerAdapter);
                    /* Set the spinner to only fire event when click on it. The event don't fire when we show the fragment*/
                    observaciones.setSelection(Adapter.NO_SELECTION, false);

                    //Look for the current state of the supply only if the estate stored is not null
                    if(auxEstado != null) {
                        cdrStates = db.query("STATE_SUPPLIES", new String[]{"_id", "ABREV_ESTADO", "ESTADOS"},
                                "ESTADOS = ?", new String[]{auxEstado},
                                null, null, null);
                        if(cdrStates.moveToFirst())
                            observaciones.setSelection(cdrStates.getInt(0)-1, true); //_id starts with index 1 so we need to subtract 1
                    }
                    /* If estado is null show that the supply is correct and avoid auxEstado as null*/
                    else {
                        observaciones.setSelection(1, true);
                        cdrStates.moveToPosition(1);
                        auxEstado = cdrStates.getString(2);
                    }
                    /* If we didn't click the spinner don't store it in the data base*/
                    estado = auxEstado;

                    /* Set listener to the state spinner and store the selected item*/
                    observaciones.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Cursor cdr = (Cursor) parent.getItemAtPosition(position);
                            estado = cdr.getString(2);
                            System.out.println("se ha cambiado el estado a: " + estado);
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                    //Populate the old lecture
                    TextView uwlant = (TextView) view.findViewById(R.id.uwlant);
                    uwlant.setText(Integer.toString(auxUWlant));

                    //Populate the consumo
                    TextView consumo = (TextView) view.findViewById(R.id.consumo);
                    consumo.setText(Integer.toString(auxConsumo));

                    //Populate the actual lecture that first of all it should be equals uwlant or cero
                    EditText uwlact = (EditText) view.findViewById(R.id.uwlact);
                    uwlact.setText(Integer.toString(auxUWlact));
                    uwlact.requestFocus();
                    uwlact.selectAll();

                    //Populate the lecture date
                    TextView uwfact = (TextView) view.findViewById(R.id.uwfact);
                    uwfact.setText(auxUWfact);

                    /* Store uwlact, uwlant and estado to pass the previous lecture the parent Activity in the interface*/
                    this.oldUWlact = auxUWlact;
                    this.uwlant = auxUWlant;
                    this.oldEstado = auxEstado;
                }
                //Close the cursor and the data base
                cdr.close();
                db.close();

            }catch (SQLiteException e){
                Toast toast = Toast.makeText(getActivity(), "Data Base unavailable", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    /* When the current supply is no longer interacting with the user save the lecture if it has been changed.
    * Do this before we go to the onStart() method of the category supply activity/fragment,
    * in order to update the supplies list correctly*/
    @Override
    public void onPause() {
        super.onPause();

        View v = getView();
        /* If the view losses the focus... (In the layout this happens only when we change supply or press back button*/
            try{
                EditText newLecture = (EditText) v.findViewById(R.id.uwlact);
                String aux = newLecture.getText().toString();
                if(!aux.equals(""))
                    uwlact = Integer.parseInt(aux);

                auxConsumo = uwlact - uwlant;
                textMessage = toTextMessage();
                subjectMessage = toSubjectMessage();

                /* Only store uwlact if it has changed*/
                if((uwlact != oldUWlact) | (!estado.equals(oldEstado))) {
                    if(uwlact == 0){
                        uwlant = uwlact;
                    }

                /* pass the function to parent Activity to share variables and tell the activity what supply to update*/
                    ((OnChangeSupplySelectedListener) activity).onChangeSupply(uwlact, uwlant, estado, supplyNo);
                }
            }catch (ClassCastException e){};
        //Set text message of the e-mail

    }

    /* If we take a picture photo and we have stored the pictured...*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Volviendo de la Cámara");

        if(requestCode == REQUEST_CODE_FROM_CAMERA && resultCode == Activity.RESULT_OK){
            try{
//                imagePath = fileUri.getPath();
                fromEmail();
            }catch (NullPointerException e){e.printStackTrace();}
        }
    }

    /* Store information when fragment is destroyed. It happens typically when the camera starts with intentions*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.d("SupplyActivity", "Entrando en onSaveInstanveState del fragment");
        if(fileUri != null) {
            outState.putParcelable("file_uri", fileUri);
            outState.putString("textMessage", textMessage);
            outState.putString("subjectMessage", subjectMessage);
            System.out.println("Entrando en onSaveInstanveState. fileUri: " + fileUri.getPath());
        }
    }

    /* Recover information when we come back from camera*/
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            fileUri = savedInstanceState.getParcelable("file_uri");
            textMessage = savedInstanceState.getString("textMessage");
            subjectMessage = savedInstanceState.getString("subjectMessage");
        }
    }

    /* Create a Listener that it is call when the view losses the focus*/
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
    }

    /* Configure e-mail and send it via intents*/
    private void fromEmail(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        String email = sharedPreferences.getString("preferences_email", getResources().getString(R.string.administrator_email));
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("application/Image");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subjectMessage);
        emailIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        emailIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
        startActivity(emailIntent);
    }

    /* Configure the name of the future picture and start the camera via intents*/
    private void fromCamera(){

        //Variables use to test if this function is avaliable on the phone
        boolean isStorageWritable = android.os.Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);

        try{
            if(!isStorageWritable) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);
                Log.d("ActivitySupply", fileUri.getPath());
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                //Start the image capture Intent
                startActivityForResult(intent, REQUEST_CODE_FROM_CAMERA);
            }else{
                Toast.makeText(getActivity(), R.string.storage_writable, Toast.LENGTH_LONG).show();
            }
        }catch (RuntimeException e){
            Toast.makeText(getActivity(), R.string.camera_failure, Toast.LENGTH_LONG).show();
        }
    }

    /* Get output media file in Uri format */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /* Set the name of the media file and the place where is going to be stored */
    private static File getOutputMediaFile(int type){

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "Gestiaguas");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                Log.d("Gestiaguas", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    /* Set the the supply selected to show information */
    public void setSupplyNo(int supplyNo){
        this.supplyNo = supplyNo;
    }

    /* Convert into String information about supply to send via e-mail*/
    private String toTextMessage(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        return getResources().getString(R.string.text_message_site) + '\n' + auxAbrevTipoVia + " " + auxCalle +'\n' +
                getResources().getString(R.string.text_message_site_number) + " " + auxNum + '\n' +
//                getResources().getString(R.string.text_message_site_local) + " " + auxLocal + '\n' +
//                getResources().getString(R.string.text_message_site_portal) + " " + auxPortal + '\n' +
//                getResources().getString(R.string.text_message_site_bloque) + " " + auxBloque + '\n' +
//                getResources().getString(R.string.text_message_site_escalera) + " " + auxEscalera + '\n' +
//                getResources().getString(R.string.text_message_site_planta) + " " + auxPlanta + '\n' +
//                getResources().getString(R.string.text_message_site_letra) + " " + auxLetra + '\n' +
                getResources().getString(R.string.text_message_site_calibre) + " " + auxCalibre + '\n' +
                getResources().getString(R.string.text_message_site_lectura) + " " + uwlact + '\n' +
                getResources().getString(R.string.text_message_site_consumo) + " " + auxConsumo + " m3" + '\n' +
                getResources().getString(R.string.text_message_site_fecha) + " " + dateFormat.format(new Date()) + '\n' + '\n';
    }

    /* Convert into String estate and  contador number to send as subject via e-mail*/
    private String toSubjectMessage(){
        return getResources().getString(R.string.subject_message_estado) + " " + estado + ".\n" +
                getResources().getString(R.string.subject_message_number_contador) + " " + auxNumContador + ".\n";
    }
}
