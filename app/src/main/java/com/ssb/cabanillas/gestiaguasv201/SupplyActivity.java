package com.ssb.cabanillas.gestiaguasv201;

import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.ssb.cabanillas.gestiaguasv201.database.CSupplyDataBaseHelper;
import com.ssb.cabanillas.gestiaguasv201.database.DatabaseContract;
import com.ssb.cabanillas.gestiaguasv201.database.GAProvider;
import com.ssb.cabanillas.gestiaguasv201.fragment.SupplyFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SupplyActivity extends AppCompatActivity implements SupplyFragment.OnChangeSupplySelectedListener {

    private float init_x; //Stores the positión touch by the user in the x axis
    private final int SENSIBILITY = 200; //The sensibility of the swipe detected

    private int supplyNo;
    private ArrayList<Integer> supplyNoList;
    private int arrayListIndex;
    private String selectedStreet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supply);

        Log.d("SupplyActivity", "On Create supplyActivity ");

        /* We set the up button in the action bar*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        //Force to show soft keyboard
//        InputMethodManager imgr = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        //Get the supply from the intent
        supplyNo = (Integer) getIntent().getExtras().get(DatabaseContract.EXTRA_SUPPLYNO);
        supplyNoList = (ArrayList<Integer>) getIntent().getExtras().get(DatabaseContract.ARRAY_SUPPLYNO);
        selectedStreet = (String) getIntent().getExtras().get(DatabaseContract.SELECTEDSTREET);
        arrayListIndex = supplyNoList.indexOf(supplyNo);

        /* Populate the the index supplies field to help user navigate through the supplies*/
        TextView showIndex = (TextView)findViewById(R.id.supply_index);
        showIndex.setText("(" + (arrayListIndex+1) + "/" + supplyNoList.size() + ")");

        //Show the supply selected in the fragment
        SupplyFragment details = new SupplyFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        //Tell the new framgment the Supply that it should show on screen
        details.setSupplyNo(supplyNo);
        ft.replace(R.id.detail_frag, details).addToBackStack(LAYOUT_INFLATER_SERVICE).commit();
    }

    /* We implement de back button  on action bar to go back to parent activity
     * when it is selected because we are in compatibility mode*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                //app icon in action bar clicked; go to parent activity
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* Drive touch event in the screen to slide between supplies*/
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = MotionEventCompat.getActionMasked(event);

        switch (action){
            case (MotionEvent.ACTION_DOWN): //When user touch the screen...
                init_x = event.getX(); //Stores the point in the x axis where the user has touch the screen
                return  true;
            case (MotionEvent.ACTION_UP): //When user release the screen...
                float distance = init_x - event.getX(); //does the user swipe the screen? we also need to set sensibility

                if(distance > 0 & Math.abs(distance) > SENSIBILITY){
                    arrayListIndex++;
                    if(arrayListIndex < supplyNoList.size()) {
                        changeSupply(true);
                    }
                    else{
                        arrayListIndex--;
                        Toast toast = Toast.makeText(this, (getResources().getString(R.string.last_supply_in_street) + " " + selectedStreet), Toast.LENGTH_SHORT);
                        /* Center the Toast text*/
                        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                        if( v != null) v.setGravity(Gravity.CENTER);
                            toast.show();
                    }
                }
                if(distance < 0 & Math.abs(distance) > SENSIBILITY){
                    arrayListIndex--;
                    if(arrayListIndex >= 0) {
                        changeSupply(false);
                    }
                    else{
                        arrayListIndex++;
                        Toast toast = Toast.makeText(this, (getResources().getString(R.string.first_supply_in_street) + " " + selectedStreet), Toast.LENGTH_SHORT);
                         /* Center the Toast text*/
                        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                        if( v != null) v.setGravity(Gravity.CENTER);
                            toast.show();
                    }
                }
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }

    /* Change the fragment supply setting the respective transition*/
    private void changeSupply(boolean side){
        /* We show the actual index and the number of supplies of the street in the activity view,
        * so we easily can see how many registers left in the street*/
        TextView showIndex = (TextView)findViewById(R.id.supply_index);
        showIndex.setText("(" + (arrayListIndex+1) + "/" + supplyNoList.size() + ")");

        //Set the supply number to show correct information
        SupplyFragment details = new SupplyFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        details.setSupplyNo(supplyNoList.get(arrayListIndex));

        /* Choose short of transition according to go previous or next supply */
        if(!side)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        else
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

        // Replace Fragment
        ft.replace(R.id.detail_frag, details).addToBackStack(LAYOUT_INFLATER_SERVICE).commit();
    }

/* Override SupplyFragment interface method to get the previous lecture "uwlact"
* This method is called when user try to change fragment or press back button*/
    @Override
    public void onChangeSupply(int uwlact, int oldUWlact, String estado, int supplyNoToSave) {

        storedPreviousSupplyIfHasBeenChanged(uwlact, oldUWlact, estado, supplyNoToSave);
        Log.d("SupplyActivity", "consumo almacedado: " + uwlact);
    }

    /*Store the previous supply in the database if the uwlact has been changed*/
    private void storedPreviousSupplyIfHasBeenChanged(int uwlact, int uwlant, String estado, int supplyNoToSave){

        try {
        /* Creating an reference to the dataBase*/
            SQLiteOpenHelper SupplyDataBaseHelper = new CSupplyDataBaseHelper(this);
            SQLiteDatabase db = SupplyDataBaseHelper.getWritableDatabase();

            /*Helps to insert date format in "uwfact" field*/
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            ContentValues newSupplyValues = new ContentValues();
            newSupplyValues.put(DatabaseContract.Supplies.UWLACT, uwlact);
            newSupplyValues.put(DatabaseContract.Supplies.UWFACT, dateFormat.format(new Date()));
            newSupplyValues.put(DatabaseContract.Supplies.CONSUMO, (uwlact - uwlant));
            newSupplyValues.put(DatabaseContract.Supplies.ESTADOS, estado);

            db.update(DatabaseContract.Supplies.TABLE_NAME,
                    newSupplyValues, DatabaseContract.Supplies._ID + " = ?",
                    new String[]{Integer.toString(supplyNoToSave)});

            /**
             *  We need to update de number of supplies completed in the street
             * */
            //Look for the name of the selected street
            Cursor cursor = db.query(DatabaseContract.Supplies.TABLE_NAME,
                    new String[]{DatabaseContract.Supplies._ID, DatabaseContract.Supplies.NOMBRE_CALLE},
                    DatabaseContract.Supplies._ID +" = ?",
                    new String[] { Integer.toString(supplyNoToSave) },
                    null, null, null);
            cursor.moveToNext();

            //Calculate the supplies completed in the street selected
            cursor = db.rawQuery("SELECT NOMBRE_CALLE, COUNT(UWFACT) FROM SUPPLIES WHERE NOMBRE_CALLE = '" + cursor.getString(1) + "'", null);
            cursor.moveToNext();
            Log.d("SupplyActivity", "Calle : " + cursor.getString(0) + ", Leídos: " + cursor.getInt(1));

            //Update the "NUM_COMPLETED" field with the number of supplies already completed in the street
            ContentValues updateNumCompleted = new ContentValues();
            updateNumCompleted.put(DatabaseContract.Streets.NUM_COMPLETED, cursor.getInt(1));

            //Use of content provider in order to update STREETS database.
            ContentResolver cr = this.getContentResolver();
            Uri uriPath = Uri.parse(GAProvider.STREETS_TABLE_URI);
            cr.update(uriPath, updateNumCompleted, null, new String[]{cursor.getString(0)});

            //Close the database
            db.close();
        }catch (SQLiteException e){
            Toast toast = Toast.makeText(this, "Register could have not been updated", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
