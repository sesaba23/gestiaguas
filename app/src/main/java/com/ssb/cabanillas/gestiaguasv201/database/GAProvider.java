package com.ssb.cabanillas.gestiaguasv201.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by sergio on 17/04/16.
 */
public class GAProvider extends ContentProvider {

    public static final String authority = "com.ssb.cabanillas.gestiaguasv201.database.GAProvider";
    private UriMatcher mUriMatcher;

    private SQLiteDatabase mDB;

    private static final int STREETS_TABLE = 0;
    private static final int STREETS_TABLE_ROW = 1;

    public static final String STREETS_TABLE_URI = "content://" + GAProvider.authority + "/" + DatabaseContract.Streets.TABLE_NAME;
    public static final String STREETS_SELECTED_URI = "content://" + GAProvider.authority + "/" + DatabaseContract.Streets.TABLE_NAME + "/";


    @Override
    public boolean onCreate() {

        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(authority, DatabaseContract.Streets.TABLE_NAME, STREETS_TABLE);
        mUriMatcher.addURI(authority, DatabaseContract.Streets.TABLE_NAME+"/#", STREETS_TABLE_ROW);

        getDBReference();

        return true;
    }

    /**
     * Update the reference to the database in order to have data up to date in every use of content provider
     */
    private void getDBReference(){
        try {
            mDB = new CSupplyDataBaseHelper(getContext()).getWritableDatabase();
        }catch (SQLiteException e) {
            /* If access to the data base fails show a message*/
            Toast toast = Toast.makeText(getContext(), "Data Base unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
       getDBReference();
        switch (mUriMatcher.match(uri)){
            case STREETS_TABLE:
                //Query STREETS table
                Cursor cursor = mDB.query(DatabaseContract.Streets.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                return cursor;

            case STREETS_TABLE_ROW:
                //Query STREETS table by id
                selection = DatabaseContract.Streets._ID + " LIKE " +uri.getLastPathSegment();
                return mDB.query(DatabaseContract.Streets.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

            default:
                return null;
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        getDBReference();
        mDB.insert(DatabaseContract.Streets.TABLE_NAME, null, values);
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        getDBReference();
        switch (mUriMatcher.match(uri)) {
            case STREETS_TABLE:
                mDB.update(DatabaseContract.Streets.TABLE_NAME,
                        values, DatabaseContract.Streets.NOMBRE_CALLE + " = ?",
                        selectionArgs);
                getContext().getContentResolver().notifyChange(Uri.parse(GAProvider.STREETS_TABLE_URI), null);
                return 1;
            default:
                return 0;
        }
    }
}
