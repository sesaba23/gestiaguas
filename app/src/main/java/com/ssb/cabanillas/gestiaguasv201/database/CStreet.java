package com.ssb.cabanillas.gestiaguasv201.database;

/**
 * Created by sergio on 12/02/16.
 */
public class CStreet {

    private int id;
    private String abrevTipoVia;
    private String nombreCalle;
    private String urbanizacion;
    private int lecturasCompletadas;
    private int suministrosCalle;
    private int calleterminada;

    public CStreet(int id, String abrevTipoVia, String nombreCalle, String urbanizacion, int calleterminada, int lecturasCompletadas, int suministrosCalle) {
        this.id = id;
        this.abrevTipoVia = abrevTipoVia;
        this.urbanizacion = urbanizacion;
        this.nombreCalle = nombreCalle;
        this.lecturasCompletadas = lecturasCompletadas;
        this.suministrosCalle = suministrosCalle;
        this.calleterminada = calleterminada;
    }

    public int getId() {
        return id;
    }

    public String getAbrevTipoVia() {
        return abrevTipoVia;
    }

    public String getNombreCalle() {
        return nombreCalle;
    }

    public String getUrbanizacion() {
        return urbanizacion;
    }

    public int getLecturasCompletadas() {
        return lecturasCompletadas;
    }

    public int getSuministrosCalle() {
        return suministrosCalle;
    }

    public void setCalleterminada(int calleterminada) {
        this.calleterminada = calleterminada;
    }

    public int isCalleterminada() {
        return calleterminada;
    }
}
