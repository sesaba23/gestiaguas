package com.ssb.cabanillas.gestiaguasv201.database;

import android.database.Cursor;
import android.util.Log;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ESTA BASE DE DATOS BEBE DE GESTIAGUAS V.1.0 (MICROSOFT ACCESS)
 *
 * Clase que realiza la conexión a la Base de Datos y Proporciona los métodos necesarios
 * para interactuar con la misma
 *
 * ¡¡¡ OJO !!! No olvidar añadir el driver jDBC a las librerias del proyecto
 *
 * @author Sergio Sánchez Barahona
 * @version Gestiaguas v2.0.1
 * @since 18/02/2016
 */
public class BDMicrosoftAccessDAO {

    private java.sql.Connection conexion;
    private java.sql.Statement sentenciaSQL, sentenciaSQL2;
    private java.sql.ResultSet cdr; // conjunto de resultados

    public BDMicrosoftAccessDAO() throws ClassNotFoundException, SQLException,
            InstantiationException, IllegalAccessException
    {
        // Cargar el controlador JDBC.
        String controller = "com.mysql.jdbc.Driver";
        Class.forName(controller).newInstance();

        conectar(); //Conectar con la fuente de datos
    }

    /**
     * Método que proporciona la conexión a la Base de Datos Gestiaguas
     * @throws SQLException
     */
    public final void conectar() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        String URL_bd = "jdbc:mysql://192.168.2.161:3306/gestiaguas-access";
        String usuario = "sergio";
        String contraseña = "tusientras08";

        // Conectar con la BD
        conexion = java.sql.DriverManager.getConnection(URL_bd, usuario, contraseña);
        // Crear una sentencia SQL
        sentenciaSQL = conexion.createStatement(
                java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        sentenciaSQL2 = conexion.createStatement(
                java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);

        Log.d("DBMicrosoftAccessDAO", "Conexión realizada con éxito");
    }

    /**
     * Cierra la conexión a la base de datos liberando recursos
     * @throws SQLException
     */
    public void cerrarConexion() throws SQLException
    {
        if (cdr != null) cdr.close();
        if (sentenciaSQL != null) sentenciaSQL.close();
        if (conexion != null) conexion.close();
        Log.d("DBMicrosoftAccessDAO", "Conexión Cerrada");
    }

    /**
     * Método que muestra un listado de las calles del municipio
     * @return ResulSet   Conjunto de Resultados con el listado calles
     * @throws SQLException
     */
    public java.sql.ResultSet obtenerListadoDeCalles() throws SQLException
    {
        cdr = sentenciaSQL.executeQuery("SELECT ABREV_TIPO_VIA, CALLES AS NOMBRE_CALLE, POLIGONO, URBANIZACION FROM"
                + " CALLES, poligonos, tipos_via WHERE CALLES.COD_POL = poligonos.COD_POL and "
                + "tipos_via.COD_TIPO_VIA = CALLES.COD_TIPO_VIA ORDER BY NOMBRE_CALLE");
        return cdr;
    }

    /**
     * Método que muestra un listado de las calles del municipio CON EL NÚMERO TOTAL DE SUMINISTROS POR CALLE
     * @return ResulSet   Conjunto de Resultados con el listado calles
     * @throws SQLException
     */
    public java.sql.ResultSet obtenerListadoDeCallesAgrupadas() throws SQLException
    {
        cdr = sentenciaSQL.executeQuery("SELECT ABREV_TIPO_VIA, CALLES.CALLES AS NOMBRE_CALLE, " +
                "PDA_TOTAL.CALLES, poligonos.URBANIZACION, COUNT(PDA_TOTAL.CALLES) AS NUM_SUPPLIES " +
                "FROM PDA_TOTAL LEFT JOIN (tipos_via LEFT JOIN(CALLES LEFT JOIN poligonos ON CALLES.COD_POL = poligonos.COD_POL) " +
                "ON CALLES.COD_TIPO_VIA = tipos_via.COD_TIPO_VIA) " +
                "ON PDA_TOTAL.CALLES = CALLES.CALLES " +
                "GROUP BY PDA_TOTAL.CALLES, tipos_via.ABREV_TIPO_VIA, poligonos.URBANIZACION");
        return cdr;
    }

    /**
     * Método que muestra un listado de de los suministros del municipio
     * @return ResulSet   Conjunto de Resultados con el listado calles
     * @throws SQLException
     */
    public java.sql.ResultSet obtenerSuministros() throws SQLException
    {
        cdr = sentenciaSQL.executeQuery("SELECT ID, UWNEMO AS ABREV_TIPO_VIA, CALLES AS NOMBRE_CALLE, NUM, UWLOCAL AS LOCAL, " +
                "PORTAL, BLOQUE, ESCALERA, PLANTA, PUERTA AS LETRA, PARCELA, UWNSER AS NUM_CONTADOR, UWCALI AS CALIBRE_MM, " +
                "UWLANT, UWFACT, UWLACT, UWCONS AS CONSUMO, UWINCI AS ABREV_ESTADO, ESTADOS FROM PDA_TOTAL " +
                "ORDER BY NOMBRE_CALLE, NUM, LOCAL, PORTAL, BLOQUE, ESCALERA, PLANTA, LETRA" );
        return cdr;
    }

    public java.sql.ResultSet obtenerEstados() throws SQLException
    {
        cdr = sentenciaSQL.executeQuery("SELECT ESTADOS, ABREV_ESTADO FROM estado_contador");
        return cdr;
    }

    public int  actualizarSuministrosLeidos(Cursor cursor) throws SQLException
    {
        int updatedRegister = 0; // Stores the register that have already updated
        while(cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String uwfact = cursor.getString(1);
            String uwlact = cursor.getString(2);
            int consumo = cursor.getInt(3);
            String numContador = cursor.getString(4);
            int calibre = cursor.getInt(5);
            String Estado = cursor.getString(6);

            if(sentenciaSQL.executeUpdate("UPDATE PDA_TOTAL SET UWLACT = '" + uwlact + "', UWCONS = " + consumo + ", " +
                    "UWNSER = " + numContador + ", UWCALI = " + calibre + ", ESTADOS =  '" + Estado + "', " +
                    "UWFACT = (STR_TO_DATE('" + uwfact + "', '%d-%m-%Y %H:%i:%s')) " +
                    "WHERE ID = '" + id + "' AND (UWFACT IS NULL OR UWFACT < (STR_TO_DATE('" + uwfact + "', '%d-%m-%Y %H:%i:%s')))")> 0 ){
                updatedRegister++;
            }
        }
        return updatedRegister;
    }

}
