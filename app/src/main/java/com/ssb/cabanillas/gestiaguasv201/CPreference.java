package com.ssb.cabanillas.gestiaguasv201;

import android.os.Bundle;
import android.preference.PreferenceFragment;


/**
 * Created by sergio on 17/02/16.
 */
public class CPreference extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
